const express = require('express');
const cors = require('cors');
const https = require('https');
const fs = require('fs');
const app = express();
const bodyParser = require('body-parser');
const sql = require('mssql');
const Password = require("node-php-password");
const nodemailer = require('nodemailer');
const math = require('mathjs');
var dayjs = require("dayjs");
var utc = require("dayjs/plugin/utc");
var timezone = require("dayjs/plugin/timezone");
var isBetween = require("dayjs/plugin/isBetween");
var admin = require("firebase-admin");
dayjs.extend(timezone);
dayjs.extend(utc);
dayjs.extend(isBetween);

admin.initializeApp({
  credential: admin.credential.cert("serviceAccountKey2.json"),
  databaseURL: "https://total-trac-default-rtdb.firebaseio.com"
});

const connStr = {
  user: 'sa',
  password: 'BIS@2018',
  server: '192.168.1.122',
  database: 'PORTAL',
  options: {
    "encrypt": false,
    "enableArithAbort": true,
    "connectTimeout": 300000,
    "requestTimeout": 300000,
  },
  pool: {
    max: 10,
    min: 5,
    idle: 30000,
    acquire: 100000
  }
};

const pool = new sql.ConnectionPool(connStr);
const poolConnect = pool.connect();

app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

function execSQLQuery(sqlQry, res) {
  pool.request().query(sqlQry)
  .then(result => res.json(result.recordset))
  .catch(err => res.json(err));
}

// --------------------------------------------------------- USER
app.get('/get_users', async (req, res) => {
  let senhaInput = String(req.query.senha);
  if (req.query.senha.indexOf('REPLACEHASHTAG')) {
    senhaInput = senhaInput.replace('REPLACEHASHTAG','#')
  }
  
  let temp, resGet = 0, hash = ''

  await pool.request().query(`SELECT * FROM new_usuario WHERE cpfcnpj = '${req.query.cpf}' AND ativo=1`)
  .then(result => {
    if (result.recordset.length > 0) {
      if (result.recordset[0].ativo == 1) {
        temp = [{
          name: result.recordset[0].nome,
          ids_clientes: result.recordset[0].ids_clientes,
          idUsuario: result.recordset[0].idUsuario,
          cliente: result.recordset[0].cliente,
          nomes_clientes: result.recordset[0].nomes_clientes,
          senha: result.recordset[0].senha,
          email: result.recordset[0].email,
          tipo: result.recordset[0].perfil_usuario_vizualizacao
        }];
    
        hash = temp[0].senha
        
        if (hash.indexOf('$2b$1') != -1) {
          hash = '$2y' + hash.substring(3,hash.length)
        }
        if (Password.verify(senhaInput, hash)) {
          resGet = temp;
        }
      } else {
        res.send('inativo')
      }
    } else {
      res.send('false')
    }
  })

  if (resGet == 0) {
    res.send('false')
  } else {

    await pool.request().query(`SELECT nome FROM cliente_cadastro cc inner join assoc_clientes_usuario on cc.id = id_cliente where id_usuario = ${temp[0].idUsuario}`)
    .then(result => {
      temp[0].nomes_clientes = result.recordset
    });

    let results
    await pool.request().query(`SELECT id_cliente FROM assoc_clientes_usuario WHERE id_usuario = '${temp[0].idUsuario}'`)
    .then(result => {
      results = result.recordset
    })
    let idsClientes = ``;
    for (let i = 0; i < results.length; i++) {
      if (i !== results.length - 1) {
        idsClientes += results[i].id_cliente + ',';
      } else {
        idsClientes += results[i].id_cliente;
      }
    }
    temp[0].ids_clientes = idsClientes;
    res.json(temp)
  }
});

app.get('/verify_users', async (req, res) => {  
  await pool.request().query(`SELECT * FROM new_usuario WHERE cpfcnpj = '${req.query.cpf}' and ativo = 1`)
  .then( result => {
    if (result.recordset.length > 0) {
      res.send(true)
    } else {
      res.send(false)
    }
  })
})

// --------------------------------------------------------- PASSWORD
app.get('/forget_password', async (req, res) => {
  await pool.request().query(`SELECT * FROM new_usuario WHERE cpfcnpj = '${req.query.cpf}'`)
  .then(result => {
    let temp = result.recordset;
    if (temp.length > 0) {

      let transport = nodemailer.createTransport({
        host: 'smtplw.com.br',
        port: 587,
        secure: false,
        auth: {
          user: "totaltrac2389",
          pass: "OHzBKreQ7578"
        },
        tls: { rejectUnauthorized: true }
      });

      let mailOptions = {
        from: 'naoresponda@totaltrac.com.br',
        to: `${temp[0].email}`,
        subject: 'Email de recuperação de senha',
        text: `Olá, 
        O seu código de recuperação de senha é: ${req.query.codigo}
        Este código irá expirar em 15 minutos.
      `,
      }

      //testa o servidor
      transport.verify(function (error) {
        if (error) {
          // console.log(error);
        }
      });

      //envia o email
      transport.sendMail(mailOptions, (error, info) => {
        if (error) {
        } else {
          res.json(temp);
        }
      })

    } else {
      res.send('false');
    }

  }).catch(
    err => res.send(err)
  );
});

app.get('/update_password', async (req, res) => {
  var senhaHash = Password.hash(req.query.senha, "PASSWORD_DEFAULT");

  await pool.request()
  .query(`
    UPDATE new_usuario
    SET senha = '${senhaHash}'  
    WHERE cpfcnpj = '${req.query.cpf}'
  `)
  .then(result => {
    if (result.rowsAffected > 0) {
      res.send('true');
    } else {
      res.send('false');
    }
  })
  .catch(err => res.send(err));
});

app.post('/update_user', async (req, res) => {
  var senhaHash = Password.hash(req.body.password, "PASSWORD_DEFAULT");

  await pool.request()
  .query(`
    UPDATE
      new_usuario 
    SET
      nome = '${req.body.name}',
      senha = '${senhaHash}',
      email = '${req.body.email}'
    WHERE cpfcnpj = '${req.body.login}'
  `)
  .then(result => {
    if (result.rowsAffected > 0) {
      res.send(true);
      } else {
      res.send(false);
    }
  })
  .catch(err => res.send(false));
});

// --------------------------------------------------------- FUNCTIONS
const sendNotification = async (fcm, title, body) => {
  var payload = {
    data: {
      type: '1'
    },
    notification: {
      title: title,
      body: body,
    }
  }

  var options = {
    android: {
      priority: 'high',
      timeToLive: 60 * 60 * 24
    },
    apns:{
      headers:{
        'apns-priority':"5"
      }
    },
  }
  
  admin.messaging().sendToDevice(fcm, payload, options)
}

const GetPermissaoVisualizacao = async (id_user) => {
  const result = await pool.request().query(`SELECT perfil_usuario_vizualizacao FROM new_usuario WHERE idUsuario  = '${id_user}'`);
  return result.recordset[0].perfil_usuario_vizualizacao
}

const GetClienteSerial = async (serial) => {
  const result = await pool.request().query(`select idCliente, id, idCategoria from cadastro_veiculo where serial = '${serial}'`);
  return result.recordset[0].idCliente
}

const GetNumeroChipSerial = async (serial) => {
  const result = await pool.request().query(`select numero_chip,serial,idCliente from cadastro_rastreador where serial = '${serial}'`);
  return result.recordset[0].numero_chip
}

const GetComando = async (comandoNome) => {
  const result = await pool.request().query(`select id,nome from comando_suntech where codigo like '%${comandoNome}%'`);
  return result.recordset[0].id
}

const GetComandoNome = async (comandoNome) => {
  const result = await pool.request().query(`select id,nome from comando_suntech where codigo like '%${comandoNome}%'`);
  return result.recordset[0].nome
}

const GetPlaca = async (serial) => {
  const result = await pool.request().query(`select id,placa from cadastro_veiculo where serial = '${serial}'`);
  return result.recordset[0].placa
}

const GetSerial = async (serial) => {
  const result = await pool.request().query(`select id,placa,serial from cadastro_veiculo where placa = '${serial}'`);
  return result.recordset[0].serial
}

function gerarNumeroAleatorio() {
  return math.floor(math.random() * 10000) + 1;
}

async function processNotification() {
  let msgs = []
  const amqp = require('amqplib');
  const queue = 'alertas_app'

  const conection = await amqp.connect('amqp://total:total123@192.168.1.243')
  const channel = await conection.createChannel()
  await channel.assertQueue(queue);
  await channel.consume(queue, async (msg) => {   
    let a = msg.content.toString()
    let arrayTemp = {
      placa: String(a).substring(String(a).indexOf('+') + 1, String(a).indexOf('=')),
      idCliente: String(a).substring(String(a).indexOf('#') + 1, String(a).indexOf('$')),
      idEvento: String(a).substring(String(a).indexOf('%') + 1, String(a).indexOf('¨')),
      dataGPS: String(a).substring(String(a).indexOf('&') + 1, String(a).indexOf('*')),
    }

    if (arrayTemp.placa == 'SequelizeConnectionError: Failed to connect to 192.168.1.122:1433 - Could not connect (sequence)' || arrayTemp.placa == 'OXD7E78' || arrayTemp.placa == 'ERF3145' || arrayTemp.placa == 'PPL2072' || arrayTemp.placa == 'GGX5D78' || arrayTemp.placa == 'ERA8I69' || arrayTemp.placa == 'FUC4F78' || arrayTemp.placa == 'NSA0000' || arrayTemp.placa == 'EHY6F54' || arrayTemp.placa == 'LSO2260' || arrayTemp.placa == 'EGQ6B72' || arrayTemp.placa == 'DIG6G92' || arrayTemp.placa == 'DZI4C65' || arrayTemp.placa == 'FPJ5876' || arrayTemp.placa == 'GME8B57' || arrayTemp.placa == 'RRO7G61' || arrayTemp.placa == 'RTO0D40' || arrayTemp.placa == 'PWL3C88' || arrayTemp.placa == 'FRN1A96' || arrayTemp.placa == 'QXA7B51' ||arrayTemp.placa == 'RRU3A30' ||arrayTemp.placa == 'FBZ5F37' ||arrayTemp.placa == 'BRQ2I34' ||arrayTemp.placa == 'PXY0519' ||arrayTemp.placa == 'ECH4H68' ||arrayTemp.placa == 'LBN8400' ||arrayTemp.placa == 'FPH3G49' ||arrayTemp.placa == 'PZA5B24' ||arrayTemp.placa == 'QCB3016' ||arrayTemp.placa == 'OBE8247' ||arrayTemp.placa == 'QCJ5407' ||arrayTemp.placa == 'MFE8237' ||arrayTemp.placa == 'QTN1G25' ||arrayTemp.placa == 'FOW6989' ||arrayTemp.placa == 'FHL6410' ||arrayTemp.placa == 'BKU9A47' ||arrayTemp.placa == 'JBC5A24' ||arrayTemp.placa == 'HBZ2H30' ||arrayTemp.placa == 'GCX2I99' ||arrayTemp.placa == 'EZC4F04' ||arrayTemp.placa == 'LKO7271' ||arrayTemp.placa == 'DRA2H60' ||arrayTemp.placa == 'FUL7977' ||arrayTemp.placa == 'EJY8C87' ||arrayTemp.placa == 'GFW3394' ||arrayTemp.placa == 'CHA2191' ||arrayTemp.placa == 'RMD0001' ||arrayTemp.placa == 'GIJ7131' ||arrayTemp.placa == 'SDS8B98' ||arrayTemp.placa == 'EJG0010' ||arrayTemp.placa == 'GIM5J44') {
      channel.ack(msg)
    } else {
      msgs.push(arrayTemp)
      channel.ack(msg)
    }
  });
  await channel.close()
  await conection.close()
  
  let notificate = []
  
  //USUARIOS ASSOCIADOS AO CLIENTE 
  for (let i = 0; i < msgs.length; i++) {
    await pool.request().query(`SELECT id_usuario AS idUsuario FROM assoc_clientes_usuario WHERE id_cliente = ${msgs[i].idCliente} ORDER BY id_usuario ASC`)
    .then( async (result) => {
      if (result.recordset.length > 0) {
        
        //PLACAS ASSOCIADAS AO USUARIO
        for (let y = 0; y < result.recordset.length; y++) {
          if (result.recordset[y].idUsuario != 1) {
            
            let y2 = result.recordset[y].idUsuario
            await pool.request().query(`SELECT p.placa FROM assoc_veiculos_usuarios v, cadastro_veiculo p WHERE v.id_usuario=${result.recordset[y].idUsuario} AND p.id = v.id_veiculo`)
            .then( async (results) => {
              
              for (let x = 0; x < results.recordset.length; x++) {
                if (results.recordset[x].placa == msgs[i].placa) {

                  //INSERE NOTIFICAÇÃO NO BANCO
                  await pool.request()
                  .query(`INSERT INTO NOTIFICACOES (idEvento, idCliente, placa, dataGPS, idUsuario) values ('${msgs[i].idEvento}', ${msgs[i].idCliente}, '${msgs[i].placa}', '${msgs[i].dataGPS}', ${result.recordset[y].idUsuario})`)
                  .then(result => {
                  });
                  let tempNotificate = {
                    user: y2,
                    place: results.recordset[x].placa
                  }
                  notificate.push(tempNotificate)
                }
              }

            })
          }
        }  
        
        //ENVIAR NOTIFICAÇÃO
        for (let y = 0; y < notificate.length; y++) {
          await pool.request().query(`SELECT fcm FROM login_app WHERE idUsuario=${notificate[y].user}`)
          .then(async (result) => {
            let title = msgs[i].idEvento + ' !!!'
            let body = 'O veículo ' + msgs[i].placa + ' gerou o evento: ' + msgs[i].idEvento

            for (let x = 0; x < result.recordset.length; x++) {
              await sendNotification(result.recordset[x].fcm, title + ' !!!', body)
            }  

          })
        }

      }
    })
  }

}

// --------------------------------------------------------- MONITORING

app.get('/get_pos_monitoring_trac', async (req, res) => {
  let arrayTemp = []
  await pool.request()
  .query(`
    SELECT TOP 1
      m.dataGPS,
      e.descricao AS evento,
      m.hodometro,
      IIF ( m.alertaIgnicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( m.alertaIgnicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      m.latitude,
      m.longitude,
      m.localizacao,
      m.placa,
      m.tensao,
      m.velocidade,
      m.PontoReferencia,
      m.rpm,
      m.idCliente,
      cv.etiqueta
    FROM movimento_grcad_frontend m
      LEFT JOIN cadastro_veiculo cv ON cv.id = m.id
      LEFT JOIN evento_grcad e ON e.id = m.evento
    WHERE m.placa='${req.query.place}'
    ORDER BY dataGPS DESC
  `)
  .then(async (result) => {
    if (result.recordset[0]) {
      let reference = result.recordset[0].PontoReferencia.substring(result.recordset[0].PontoReferencia.indexOf(' de ') + 4, result.recordset[0].PontoReferencia.length)
      let idCliente = result.recordset[0].idCliente
      await pool.request()
      .query(`
        SELECT TOP 1
          latitude,
          longitude,
          nomeReferencia,
          raio
        FROM ponto_referencia
        WHERE nomeReferencia = '${reference}'
          AND idCliente = ${idCliente}
      `)
      .then(async (result2) => {
        
        let odometer = await String(result.recordset[0].odometro)
        if (odometer.indexOf('.') == -1) {
          odometer = (odometer.substring(0, odometer.length - 3) + '.' + odometer.substring(odometer.length - 3, odometer.length))
        } else {
          odometer = odometer
        }
        let vKm = odometer.substring(0, odometer.indexOf('.'))
        let M = odometer.substring(odometer.indexOf('.') + 1, odometer.length)
        
        if(vKm.length > 0) {
          switch (vKm.length) {
            case 1:
              odometer = String(vKm) + ' Km'
            break
            case 2:
              odometer = String(vKm)
            break
            case 3:
              odometer = String(vKm) + ' Km'
            break
            case 4:
              odometer = String(vKm.substring(0, 1)) + '.' + String(vKm.substring(1, vKm.length)) + ' Km'
            break
            case 5:
              odometer = String(vKm.substring(0, 2)) + '.' + String(vKm.substring(2, vKm.length)) + ' Km'
            break
            case 6:
              odometer = String(vKm.substring(0, 3)) + '.' + String(vKm.substring(3, vKm.length)) + ' Km'
            break
          }
        } else {
          odometer = String(M) + ' m'
        }

        let objectTemp = {
          dataGPS: result.recordset[0].dataGPS,
          placa: result.recordset[0].placa,
          etiqueta: result.recordset[0].etiqueta,
          coords: {latitude: parseFloat(result.recordset[0].latitude), longitude: parseFloat(result.recordset[0].longitude)},
          localizacao: result.recordset[0].localizacao,
          velocidade: result.recordset[0].velocidade,
          ignicao: result.recordset[0].ignicao,
          statusVeic: result.recordset[0].statusVeic,
          evento: result.recordset[0].evento,
          odometro: odometer,
          tensao: result.recordset[0].tensao,
          rpm: result.recordset[0].rpm,
          referenciaCoords: result.recordset[0].PontoReferencia ? {latitude: parseFloat(result2.recordset[0].latitude), longitude: parseFloat(result2.recordset[0].longitude)} : null,
          referenciaNome: result.recordset[0].PontoReferencia ? result2.recordset[0].nomeReferencia : null,
          referenciaRaio: result.recordset[0].PontoReferencia ? result2.recordset[0].raio : null
        }
        arrayTemp.push(objectTemp)
      })
    } else {
      res.send(false)
    }
  })
  res.send(arrayTemp)
})

app.get('/get_pos_monitoring_teltonika', async (req, res) => {
  let arrayTemp = []
  await pool.request()
  .query(`
    SELECT TOP 1
      m.dataServidor AS dataGPS,
      e.descricao AS evento,
      m.TotalOdometro AS odometro,
      IIF ( m.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( m.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      m.latitude,
      m.longitude,
      m.endereco AS localizacao,
      m.placa,
      m.bateria_externa AS tensao,
      m.velocidade2 AS velocidade,
      m.PontoReferencia,
      m.temperaturaS2 AS temperatura,
      m.umidadeS2 AS umidade,
      m.idCliente,
      cv.etiqueta
    FROM movimento_teltonika_frontend m
      LEFT JOIN cadastro_veiculo cv ON cv.id = m.id
      LEFT JOIN evento_grcad e ON e.id = m.evento
    WHERE m.placa='${req.query.place}'
    ORDER BY dataGPS DESC
  `)
  .then(async (result) => {
    if (result.recordset[0]) {
      let reference = result.recordset[0].PontoReferencia.substring(result.recordset[0].PontoReferencia.indexOf(' de ') + 4, result.recordset[0].PontoReferencia.length)
      let idCliente = result.recordset[0].idCliente
      await pool.request()
      .query(`
        SELECT TOP 1
          latitude,
          longitude,
          nomeReferencia,
          raio
        FROM ponto_referencia
        WHERE nomeReferencia = '${reference}'
          AND idCliente = ${idCliente}
      `)
      .then(async (result2) => {
        let odometer = await String(result.recordset[0].odometro)
        if (odometer.indexOf('.') == -1) {
          odometer = (odometer.substring(0, odometer.length - 3) + '.' + odometer.substring(odometer.length - 3, odometer.length))
        } else {
          odometer = odometer
        }
        let vKm = odometer.substring(0, odometer.indexOf('.'))
        let M = odometer.substring(odometer.indexOf('.') + 1, odometer.length)
        
        if(vKm.length > 0) {
          switch (vKm.length) {
            case 1:
              odometer = String(vKm) + ' Km'
            break
            case 2:
              odometer = String(vKm)
            break
            case 3:
              odometer = String(vKm) + ' Km'
            break
            case 4:
              odometer = String(vKm.substring(0, 1)) + '.' + String(vKm.substring(1, vKm.length)) + ' Km'
            break
            case 5:
              odometer = String(vKm.substring(0, 2)) + '.' + String(vKm.substring(2, vKm.length)) + ' Km'
            break
            case 6:
              odometer = String(vKm.substring(0, 3)) + '.' + String(vKm.substring(3, vKm.length)) + ' Km'
            break
          }
        } else {
          odometer = String(M) + ' m'
        }
        
        let objectTemp = {
          dataGPS: result.recordset[0].dataGPS,
          placa: result.recordset[0].placa,
          etiqueta: result.recordset[0].etiqueta,
          coords: {latitude: parseFloat(result.recordset[0].latitude), longitude: parseFloat(result.recordset[0].longitude)},
          localizacao: result.recordset[0].localizacao,
          velocidade: result.recordset[0].velocidade,
          ignicao: result.recordset[0].ignicao,
          statusVeic: result.recordset[0].statusVeic,
          evento: result.recordset[0].evento,
          odometro: odometer,
          temperatura: result.recordset[0].temperatura,
          umidade: result.recordset[0].umidade,
          tensao: result.recordset[0].tensao,
          referenciaCoords: result.recordset[0].PontoReferencia ? {latitude: parseFloat(result2.recordset[0].latitude), longitude: parseFloat(result2.recordset[0].longitude)} : null,
          referenciaNome: result.recordset[0].PontoReferencia ? result2.recordset[0].nomeReferencia : null,
          referenciaRaio: result.recordset[0].PontoReferencia ? result2.recordset[0].raio : null
        }
        arrayTemp.push(objectTemp)
      })
    } else {
      res.send(false)
    }
  })
  res.send(arrayTemp)
})

app.get('/get_pos_monitoring_suntech', async (req, res) => {
  let arrayTemp = []
  await pool.request()
  .query(`
    SELECT TOP 1
      m.dataGPS,
      CASE 
        WHEN (tipo = 'STT') THEN (select eventDesc from eventos_suntech_stt a where evento = a.codEvento )
        WHEN (tipo = 'ALT') THEN (select eventDesc from eventos_suntech_alt a where evento = a.codEvento )
        WHEN (tipo = 'EMG') THEN (select eventoDesc from eventos_suntech_emg a where evento = a.codEvento )
        WHEN (tipo = 'EVT') THEN (select eventDesc from eventos_suntech_evt a where evento = a.codEvento )
      ELSE '' END evento,
      m.odometro,
      IIF ( m.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( m.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      m.latitude,
      m.longitude,
      m.localizacao,
      m.placa,
      m.tensao,
      CAST(CAST(m.velocidade AS FLOAT) AS INT) as velocidade,
      m.PontoReferencia,
      m.temperaturaS1,
      m.rpm,
      m.idCliente,
      cv.etiqueta
    FROM movimento_suntech_frontend m
      LEFT JOIN cadastro_veiculo cv ON cv.id = m.id
    WHERE m.placa='${req.query.place}'
    ORDER BY dataGPS DESC
  `)
  .then(async (result) => {
    if (result.recordset[0]) {
      let reference = result.recordset[0].PontoReferencia.substring(result.recordset[0].PontoReferencia.indexOf(' de ') + 4, result.recordset[0].PontoReferencia.length)
      let idCliente = result.recordset[0].idCliente
      await pool.request()
      .query(`
        SELECT TOP 1
          latitude,
          longitude,
          nomeReferencia,
          raio
        FROM ponto_referencia
        WHERE nomeReferencia = '${reference}'
          AND idCliente = ${idCliente}
      `)
      .then(async (result2) => {
        let odometer = await String(result.recordset[0].odometro)
        if (odometer.indexOf('.') == -1) {
          odometer = (odometer.substring(0, odometer.length - 3) + '.' + odometer.substring(odometer.length - 3, odometer.length))
        } else {
          odometer = odometer
        }
        let vKm = odometer.substring(0, odometer.indexOf('.'))
        let M = odometer.substring(odometer.indexOf('.') + 1, odometer.length)
        
        if(vKm.length > 0) {
          switch (vKm.length) {
            case 1:
              odometer = String(vKm) + ' Km'
            break
            case 2:
              odometer = String(vKm)
            break
            case 3:
              odometer = String(vKm) + ' Km'
            break
            case 4:
              odometer = String(vKm.substring(0, 1)) + '.' + String(vKm.substring(1, vKm.length)) + ' Km'
            break
            case 5:
              odometer = String(vKm.substring(0, 2)) + '.' + String(vKm.substring(2, vKm.length)) + ' Km'
            break
            case 6:
              odometer = String(vKm.substring(0, 3)) + '.' + String(vKm.substring(3, vKm.length)) + ' Km'
            break
          }
        } else {
          odometer = String(M) + ' m'
        }

        let objectTemp = {
          dataGPS: result.recordset[0].dataGPS,
          placa: result.recordset[0].placa,
          etiqueta: result.recordset[0].etiqueta,
          coords: {latitude: parseFloat(result.recordset[0].latitude), longitude: parseFloat(result.recordset[0].longitude)},
          localizacao: result.recordset[0].localizacao,
          velocidade: result.recordset[0].velocidade,
          ignicao: result.recordset[0].ignicao,
          statusVeic: result.recordset[0].statusVeic,
          evento: result.recordset[0].evento,
          odometro: odometer,
          tensao: result.recordset[0].tensao,
          temperatura: result.recordset[0].temperatura,
          rpm: result.recordset[0].rpm,
          referenciaCoords: result.recordset[0].PontoReferencia ? {latitude: parseFloat(result2.recordset[0].latitude), longitude: parseFloat(result2.recordset[0].longitude)} : null,
          referenciaNome: result.recordset[0].PontoReferencia ? result2.recordset[0].nomeReferencia : null,
          referenciaRaio: result.recordset[0].PontoReferencia ? result2.recordset[0].raio : null
        }
        arrayTemp.push(objectTemp)
      })
    } else {
      res.send(false)
    }
  })
  res.send(arrayTemp)
})

app.get('/get_pos_monitoring_gv57', async (req, res) => {
  let arrayTemp = []
  await pool.request()
  .query(`
    SELECT TOP 1
      FORMAT( DATEADD (HOUR, CAST(- 0 AS INTEGER), m.utctime), 'yyyy-MM-dd HH:mm:ss' ) AS dataGPS,
      e.descricao,
      m.mileage AS odometro,
      IIF ( m.ignicao = 0, 'Ligado', 'Desligado') as ignicao,
      IIF ( m.ignicao = 0, '#00EB00', '#FF0322' ) as statusVeic,
      m.latitude,
      m.longitude,
      m.localizacao,
      m.placa,
      m.externalpower AS tensao,
      m.speed AS velocidade,
      m.PontoReferencia,
      m.idCliente,
      cv.etiqueta
    FROM movimento_gv57_frontend m
      LEFT JOIN cadastro_veiculo cv ON cv.id = m.id
      LEFT JOIN evento_grcad e ON e.id = m.event
    WHERE m.placa='${req.query.place}'
    ORDER BY dataGPS DESC
  `)
  .then(async (result) => {
    if (result.recordset[0]) {
      let reference = result.recordset[0].PontoReferencia.substring(result.recordset[0].PontoReferencia.indexOf(' de ') + 4, result.recordset[0].PontoReferencia.length)
      let idCliente = result.recordset[0].idCliente
      await pool.request()
      .query(`
        SELECT TOP 1
          latitude,
          longitude,
          nomeReferencia,
          raio
        FROM ponto_referencia
        WHERE nomeReferencia = '${reference}'
          AND idCliente = ${idCliente}
      `)
      .then(async (result2) => {
        let odometer = await String(result.recordset[0].odometro)
        if (odometer.indexOf('.') == -1) {
          odometer = (odometer.substring(0, odometer.length - 3) + '.' + odometer.substring(odometer.length - 3, odometer.length))
        } else {
          odometer = odometer
        }
        let vKm = odometer.substring(0, odometer.indexOf('.'))
        let M = odometer.substring(odometer.indexOf('.') + 1, odometer.length)
        
        if(vKm.length > 0) {
          switch (vKm.length) {
            case 1:
              odometer = String(vKm) + ' Km'
            break
            case 2:
              odometer = String(vKm)
            break
            case 3:
              odometer = String(vKm) + ' Km'
            break
            case 4:
              odometer = String(vKm.substring(0, 1)) + '.' + String(vKm.substring(1, vKm.length)) + ' Km'
            break
            case 5:
              odometer = String(vKm.substring(0, 2)) + '.' + String(vKm.substring(2, vKm.length)) + ' Km'
            break
            case 6:
              odometer = String(vKm.substring(0, 3)) + '.' + String(vKm.substring(3, vKm.length)) + ' Km'
            break
          }
        } else {
          odometer = String(M) + ' m'
        }
        let objectTemp = {
          dataGPS: result.recordset[0].dataGPS,
          placa: result.recordset[0].placa,
          etiqueta: result.recordset[0].etiqueta,
          coords: {latitude: parseFloat(result.recordset[0].latitude), longitude: parseFloat(result.recordset[0].longitude)},
          localizacao: result.recordset[0].localizacao,
          velocidade: result.recordset[0].velocidade,
          ignicao: result.recordset[0].ignicao,
          statusVeic: result.recordset[0].statusVeic,
          evento: result.recordset[0].evento,
          odometro: odometer,
          tensao: result.recordset[0].tensao,
          temperatura: result.recordset[0].temperatura,
          referenciaCoords: result.recordset[0].PontoReferencia ? {latitude: parseFloat(result2.recordset[0].latitude), longitude: parseFloat(result2.recordset[0].longitude)} : null,
          referenciaNome: result.recordset[0].PontoReferencia ? result2.recordset[0].nomeReferencia : null,
          referenciaRaio: result.recordset[0].PontoReferencia ? result2.recordset[0].raio : null
        }
        arrayTemp.push(objectTemp)
      })
    } else {
      res.send(false)
    }
  })
  res.send(arrayTemp)
})

app.get('/get_pos_monitoring_gv75', async (req, res) => {
  let arrayTemp = []
  await pool.request()
  .query(`
    SELECT TOP 1
      FORMAT( DATEADD (HOUR, CAST(- 3 AS INTEGER), m.utctime), 'yyyy-MM-dd HH:mm:ss' ) AS dataGPS,
      e.descricao,
      m.mileage AS odometro,
      IIF ( m.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( m.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      m.latitude,
      m.longitude,
      m.localizacao,
      m.placa,
      m.externalpower AS tensao,
      m.speed AS velocidade,
      m.PontoReferencia,
      m.idCliente,
      cv.etiqueta
    FROM movimento_gv75_frontend m
      LEFT JOIN cadastro_veiculo cv ON cv.id = m.id
      LEFT JOIN evento_grcad e ON e.id = m.event
    WHERE m.placa='${req.query.place}'
    ORDER BY dataGPS DESC
  `)
  .then(async (result) => {
    if (result.recordset[0]) {
      let reference = result.recordset[0].PontoReferencia.substring(result.recordset[0].PontoReferencia.indexOf(' de ') + 4, result.recordset[0].PontoReferencia.length)
      let idCliente = result.recordset[0].idCliente
      await pool.request()
      .query(`
        SELECT TOP 1
          latitude,
          longitude,
          nomeReferencia,
          raio
        FROM ponto_referencia
        WHERE nomeReferencia = '${reference}'
          AND idCliente = ${idCliente}
      `)
      .then(async (result2) => {
        let odometer = await String(result.recordset[0].odometro)
        if (odometer.indexOf('.') == -1) {
          odometer = (odometer.substring(0, odometer.length - 3) + '.' + odometer.substring(odometer.length - 3, odometer.length))
        } else {
          odometer = odometer
        }
        let vKm = odometer.substring(0, odometer.indexOf('.'))
        let M = odometer.substring(odometer.indexOf('.') + 1, odometer.length)
        
        if(vKm.length > 0) {
          switch (vKm.length) {
            case 1:
              odometer = String(vKm) + ' Km'
            break
            case 2:
              odometer = String(vKm)
            break
            case 3:
              odometer = String(vKm) + ' Km'
            break
            case 4:
              odometer = String(vKm.substring(0, 1)) + '.' + String(vKm.substring(1, vKm.length)) + ' Km'
            break
            case 5:
              odometer = String(vKm.substring(0, 2)) + '.' + String(vKm.substring(2, vKm.length)) + ' Km'
            break
            case 6:
              odometer = String(vKm.substring(0, 3)) + '.' + String(vKm.substring(3, vKm.length)) + ' Km'
            break
          }
        } else {
          odometer = String(M) + ' m'
        }
        let objectTemp = {
          dataGPS: result.recordset[0].dataGPS,
          placa: result.recordset[0].placa,
          etiqueta: result.recordset[0].etiqueta,
          coords: {latitude: parseFloat(result.recordset[0].latitude), longitude: parseFloat(result.recordset[0].longitude)},
          localizacao: result.recordset[0].localizacao,
          velocidade: result.recordset[0].velocidade,
          ignicao: result.recordset[0].ignicao,
          statusVeic: result.recordset[0].statusVeic,
          evento: result.recordset[0].evento,
          odometro: odometer,
          tensao: result.recordset[0].tensao,
          temperatura: result.recordset[0].temperatura,
          referenciaCoords: result.recordset[0].PontoReferencia ? {latitude: parseFloat(result2.recordset[0].latitude), longitude: parseFloat(result2.recordset[0].longitude)} : null,
          referenciaNome: result.recordset[0].PontoReferencia ? result2.recordset[0].nomeReferencia : null,
          referenciaRaio: result.recordset[0].PontoReferencia ? result2.recordset[0].raio : null
        }
        arrayTemp.push(objectTemp)
      })
    } else {
      res.send(false)
    }
  })
  res.send(arrayTemp)
})

app.get('/get_pos_monitoring_vl01', async (req, res) => {
  let arrayTemp = []
  await pool.request()
  .query(`
    SELECT TOP 1
      m.dataGPS,
      e.descricao AS evento,
      m.odometro,
      IIF ( m.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( m.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      m.latitude,
      m.longitude,
      m.endereco,
      m.placa,
      m.tensao_bateria_externa AS tensao,
      m.velocidade,
      m.PontoReferencia,
      m.id_cliente AS idCliente,
      cv.etiqueta
    FROM movimento_vl01_frontend m
      LEFT JOIN cadastro_veiculo cv ON cv.id = m.id
      LEFT JOIN evento_grcad e ON e.id = m.evento
    WHERE m.placa='${req.query.place}'
    ORDER BY dataGPS DESC
  `)
  .then(async (result) => {
    if (result.recordset[0]) {
      let reference = result.recordset[0].PontoReferencia.substring(result.recordset[0].PontoReferencia.indexOf(' de ') + 4, result.recordset[0].PontoReferencia.length)
      let idCliente = result.recordset[0].idCliente
      await pool.request()
      .query(`
        SELECT TOP 1
          latitude,
          longitude,
          nomeReferencia,
          raio
        FROM ponto_referencia
        WHERE nomeReferencia = '${reference}'
          AND idCliente = ${idCliente}
      `)
      .then(async (result2) => {
        let odometer = await String(result.recordset[0].odometro)
        if (odometer.indexOf('.') == -1) {
          odometer = (odometer.substring(0, odometer.length - 3) + '.' + odometer.substring(odometer.length - 3, odometer.length))
        } else {
          odometer = odometer
        }
        let vKm = odometer.substring(0, odometer.indexOf('.'))
        let M = odometer.substring(odometer.indexOf('.') + 1, odometer.length)
        
        if(vKm.length > 0) {
          switch (vKm.length) {
            case 1:
              odometer = String(vKm) + ' Km'
            break
            case 2:
              odometer = String(vKm)
            break
            case 3:
              odometer = String(vKm) + ' Km'
            break
            case 4:
              odometer = String(vKm.substring(0, 1)) + '.' + String(vKm.substring(1, vKm.length)) + ' Km'
            break
            case 5:
              odometer = String(vKm.substring(0, 2)) + '.' + String(vKm.substring(2, vKm.length)) + ' Km'
            break
            case 6:
              odometer = String(vKm.substring(0, 3)) + '.' + String(vKm.substring(3, vKm.length)) + ' Km'
            break
          }
        } else {
          odometer = String(M) + ' m'
        }
        let objectTemp = {
          dataGPS: result.recordset[0].dataGPS,
          placa: result.recordset[0].placa,
          etiqueta: result.recordset[0].etiqueta,
          coords: {latitude: parseFloat(result.recordset[0].latitude), longitude: parseFloat(result.recordset[0].longitude)},
          localizacao: result.recordset[0].localizacao,
          velocidade: result.recordset[0].velocidade,
          ignicao: result.recordset[0].ignicao,
          statusVeic: result.recordset[0].statusVeic,
          evento: result.recordset[0].evento,
          odometro: odometer,
          tensao: result.recordset[0].tensao,
          temperatura: result.recordset[0].temperatura,
          referenciaCoords: result.recordset[0].PontoReferencia ? {latitude: parseFloat(result2.recordset[0].latitude), longitude: parseFloat(result2.recordset[0].longitude)} : null,
          referenciaNome: result.recordset[0].PontoReferencia ? result2.recordset[0].nomeReferencia : null,
          referenciaRaio: result.recordset[0].PontoReferencia ? result2.recordset[0].raio : null
        }
        arrayTemp.push(objectTemp)
      })
    } else {
      res.send(false)
    }
  })
  res.send(arrayTemp)
})

app.get('/get_pos_monitoring_vl02', async (req, res) => {
  let arrayTemp = []
  await pool.request()
  .query(`
    SELECT TOP 1
      m.dataGPS,
      e.event_desc AS evento,
      m.odometro,
      IIF ( m.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( m.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      m.latitude,
      m.longitude,
      m.endereco,
      m.placa,
      m.tensao_bateria_externa AS tensao,
      m.velocidade,
      m.PontoReferencia,
      m.id_cliente AS idCliente,
      m.temperaturaS1 as temperatura,
      cv.etiqueta
    FROM movimento_vl02_frontend m
      LEFT JOIN cadastro_veiculo cv ON cv.id = m.id
      LEFT JOIN evento_vl02 e ON e.id = m.evento
    WHERE m.placa='${req.query.place}'
    ORDER BY dataGPS DESC
  `)
  .then(async (result) => {
    if (result.recordset[0]) {
      let reference = result.recordset[0].PontoReferencia.substring(result.recordset[0].PontoReferencia.indexOf(' de ') + 4, result.recordset[0].PontoReferencia.length)
      let idCliente = result.recordset[0].idCliente
      await pool.request()
      .query(`
        SELECT TOP 1
          latitude,
          longitude,
          nomeReferencia,
          raio
        FROM ponto_referencia
        WHERE nomeReferencia = '${reference}'
          AND idCliente = ${idCliente}
      `)
      .then(async (result2) => {
        let odometer = await String(result.recordset[0].odometro)
        if (odometer.indexOf('.') == -1) {
          odometer = (odometer.substring(0, odometer.length - 3) + '.' + odometer.substring(odometer.length - 3, odometer.length))
        } else {
          odometer = odometer
        }
        let vKm = odometer.substring(0, odometer.indexOf('.'))
        let M = odometer.substring(odometer.indexOf('.') + 1, odometer.length)
        
        if(vKm.length > 0) {
          switch (vKm.length) {
            case 1:
              odometer = String(vKm) + ' Km'
            break
            case 2:
              odometer = String(vKm)
            break
            case 3:
              odometer = String(vKm) + ' Km'
            break
            case 4:
              odometer = String(vKm.substring(0, 1)) + '.' + String(vKm.substring(1, vKm.length)) + ' Km'
            break
            case 5:
              odometer = String(vKm.substring(0, 2)) + '.' + String(vKm.substring(2, vKm.length)) + ' Km'
            break
            case 6:
              odometer = String(vKm.substring(0, 3)) + '.' + String(vKm.substring(3, vKm.length)) + ' Km'
            break
          }
        } else {
          odometer = String(M) + ' m'
        }
        let objectTemp = {
          dataGPS: result.recordset[0].dataGPS,
          placa: result.recordset[0].placa,
          etiqueta: result.recordset[0].etiqueta,
          coords: {latitude: parseFloat(result.recordset[0].latitude), longitude: parseFloat(result.recordset[0].longitude)},
          localizacao: result.recordset[0].endereco,
          velocidade: result.recordset[0].velocidade,
          ignicao: result.recordset[0].ignicao,
          statusVeic: result.recordset[0].statusVeic,
          evento: result.recordset[0].evento,
          odometro: odometer,
          tensao: result.recordset[0].tensao < 1 || result.recordset[0].tensao == null ? result.recordset[0].tensao : 0,
          temperatura: result.recordset[0].temperatura != 0 ? result.recordset[0].temperatura : undefined,
          referenciaCoords: result.recordset[0].PontoReferencia ? {latitude: parseFloat(result2.recordset[0].latitude), longitude: parseFloat(result2.recordset[0].longitude)} : null,
          referenciaNome: result.recordset[0].PontoReferencia ? result2.recordset[0].nomeReferencia : null,
          referenciaRaio: result.recordset[0].PontoReferencia ? result2.recordset[0].raio : null
        }
        arrayTemp.push(objectTemp)
      })
    } else {
      res.send(false)
    }
  })
  res.send(arrayTemp)
})

app.get('/get_pos_monitoring_vl03', async (req, res) => {
  let arrayTemp = []
  await pool.request()
  .query(`
    SELECT TOP 1
      m.dataGPS,
      e.descricao AS evento,
      m.odometro,
      IIF ( m.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( m.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      m.latitude,
      m.longitude,
      m.endereco,
      m.placa,
      m.tensao_bateria_externa AS tensao,
      m.velocidade,
      m.PontoReferencia,
      m.id_cliente AS idCliente,
      cv.etiqueta
    FROM movimento_suntech_frontend m
      LEFT JOIN cadastro_veiculo cv ON cv.id = m.id
      LEFT JOIN evento_grcad e ON e.id = m.evento
    WHERE m.placa='${req.query.place}'
    ORDER BY dataGPS DESC
  `)
  .then(async (result) => {
    if (result.recordset[0]) {
      let reference = result.recordset[0].PontoReferencia.substring(result.recordset[0].PontoReferencia.indexOf(' de ') + 4, result.recordset[0].PontoReferencia.length)
      let idCliente = result.recordset[0].idCliente
      await pool.request()
      .query(`
        SELECT TOP 1
          latitude,
          longitude,
          nomeReferencia,
          raio
        FROM ponto_referencia
        WHERE nomeReferencia = '${reference}'
          AND idCliente = ${idCliente}
      `)
      .then(async (result2) => {
        let odometer = await String(result.recordset[0].odometro)
        if (odometer.indexOf('.') == -1) {
          odometer = (odometer.substring(0, odometer.length - 3) + '.' + odometer.substring(odometer.length - 3, odometer.length))
        } else {
          odometer = odometer
        }
        let vKm = odometer.substring(0, odometer.indexOf('.'))
        let M = odometer.substring(odometer.indexOf('.') + 1, odometer.length)
        
        if(vKm.length > 0) {
          switch (vKm.length) {
            case 1:
              odometer = String(vKm) + ' Km'
            break
            case 2:
              odometer = String(vKm)
            break
            case 3:
              odometer = String(vKm) + ' Km'
            break
            case 4:
              odometer = String(vKm.substring(0, 1)) + '.' + String(vKm.substring(1, vKm.length)) + ' Km'
            break
            case 5:
              odometer = String(vKm.substring(0, 2)) + '.' + String(vKm.substring(2, vKm.length)) + ' Km'
            break
            case 6:
              odometer = String(vKm.substring(0, 3)) + '.' + String(vKm.substring(3, vKm.length)) + ' Km'
            break
          }
        } else {
          odometer = String(M) + ' m'
        }
        let objectTemp = {
          dataGPS: result.recordset[0].dataGPS,
          placa: result.recordset[0].placa,
          etiqueta: result.recordset[0].etiqueta,
          coords: {latitude: parseFloat(result.recordset[0].latitude), longitude: parseFloat(result.recordset[0].longitude)},
          localizacao: result.recordset[0].localizacao,
          velocidade: result.recordset[0].velocidade,
          ignicao: result.recordset[0].ignicao,
          statusVeic: result.recordset[0].statusVeic,
          evento: result.recordset[0].evento,
          odometro: odometer,
          tensao: result.recordset[0].tensao,
          temperatura: result.recordset[0].temperatura,
          referenciaCoords: result.recordset[0].PontoReferencia ? {latitude: parseFloat(result2.recordset[0].latitude), longitude: parseFloat(result2.recordset[0].longitude)} : null,
          referenciaNome: result.recordset[0].PontoReferencia ? result2.recordset[0].nomeReferencia : null,
          referenciaRaio: result.recordset[0].PontoReferencia ? result2.recordset[0].raio : null
        }
        arrayTemp.push(objectTemp)
      })
    } else {
      res.send(false)
    }
  })
  res.send(arrayTemp)
})

app.get('/get_pos_monitoring_mt2000', async (req, res) => {
  let arrayTemp = []
  await pool.request()
  .query(`
    SELECT TOP 1
      m.dataGPS,
      e.descricao AS evento,
      m.ODEMETER_TOTAL AS odometro,
      IIF ( m.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( m.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      m.latitude,
      m.longitude,
      m.localizacao,
      m.placa,
      m.tensao,
      m.velocidade,
      m.PontoReferencia,
      m.idCliente,
      cv.etiqueta
    FROM movimento_mt2000_frontend m
      LEFT JOIN cadastro_veiculo cv ON cv.id = m.id
      LEFT JOIN evento_grcad e ON e.id = m.evento
    WHERE m.placa='${req.query.place}'
    ORDER BY dataGPS DESC
  `)
  .then(async (result) => {
    if (result.recordset[0]) {
      let reference = result.recordset[0].PontoReferencia.substring(result.recordset[0].PontoReferencia.indexOf(' de ') + 4, result.recordset[0].PontoReferencia.length)
      let idCliente = result.recordset[0].idCliente
      await pool.request()
      .query(`
        SELECT TOP 1
          latitude,
          longitude,
          nomeReferencia,
          raio
        FROM ponto_referencia
        WHERE nomeReferencia = '${reference}'
          AND idCliente = ${idCliente}
      `)
      .then(async (result2) => {
        let odometer = await String(result.recordset[0].odometro)
        if (odometer.indexOf('.') == -1) {
          odometer = (odometer.substring(0, odometer.length - 3) + '.' + odometer.substring(odometer.length - 3, odometer.length))
        } else {
          odometer = odometer
        }
        let vKm = odometer.substring(0, odometer.indexOf('.'))
        let M = odometer.substring(odometer.indexOf('.') + 1, odometer.length)
        
        if(vKm.length > 0) {
          switch (vKm.length) {
            case 1:
              odometer = String(vKm) + ' Km'
            break
            case 2:
              odometer = String(vKm)
            break
            case 3:
              odometer = String(vKm) + ' Km'
            break
            case 4:
              odometer = String(vKm.substring(0, 1)) + '.' + String(vKm.substring(1, vKm.length)) + ' Km'
            break
            case 5:
              odometer = String(vKm.substring(0, 2)) + '.' + String(vKm.substring(2, vKm.length)) + ' Km'
            break
            case 6:
              odometer = String(vKm.substring(0, 3)) + '.' + String(vKm.substring(3, vKm.length)) + ' Km'
            break
          }
        } else {
          odometer = String(M) + ' m'
        }
        let objectTemp = {
          dataGPS: result.recordset[0].dataGPS,
          placa: result.recordset[0].placa,
          etiqueta: result.recordset[0].etiqueta,
          coords: {latitude: parseFloat(result.recordset[0].latitude), longitude: parseFloat(result.recordset[0].longitude)},
          localizacao: result.recordset[0].localizacao,
          velocidade: result.recordset[0].velocidade,
          ignicao: result.recordset[0].ignicao,
          statusVeic: result.recordset[0].statusVeic,
          evento: result.recordset[0].evento,
          odometro: odometer,
          tensao: result.recordset[0].tensao,
          temperatura: result.recordset[0].temperatura,
          referenciaCoords: result.recordset[0].PontoReferencia ? {latitude: parseFloat(result2.recordset[0].latitude), longitude: parseFloat(result2.recordset[0].longitude)} : null,
          referenciaNome: result.recordset[0].PontoReferencia ? result2.recordset[0].nomeReferencia : null,
          referenciaRaio: result.recordset[0].PontoReferencia ? result2.recordset[0].raio : null
        }
        arrayTemp.push(objectTemp)
      })
    } else {
      res.send(false)
    }
  })
  res.send(arrayTemp)
})

app.get('/get_pos_monitoring_st390', async (req, res) => {
  let arrayTemp = []
  await pool.request()
  .query(`
    SELECT TOP 1
      m.dataGPS,
      e.descricao AS evento,
      m.odometro,
      IIF ( m.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( m.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      m.latitude,
      m.longitude,
      m.localizacao,
      m.placa,
      m.tensao,
      m.velocidade,
      m.PontoReferencia,
      m.idCliente,
      cv.etiqueta
    FROM movimento_mt2000_frontend m
      LEFT JOIN cadastro_veiculo cv ON cv.id = m.id
      LEFT JOIN evento_grcad e ON e.id = m.evento
    WHERE m.placa='${req.query.place}'
    ORDER BY dataGPS DESC
  `)
  .then(async (result) => {
    if (result.recordset[0]) {
      let reference = result.recordset[0].PontoReferencia.substring(result.recordset[0].PontoReferencia.indexOf(' de ') + 4, result.recordset[0].PontoReferencia.length)
      let idCliente = result.recordset[0].idCliente
      await pool.request()
      .query(`
        SELECT TOP 1
          latitude,
          longitude,
          nomeReferencia,
          raio
        FROM ponto_referencia
        WHERE nomeReferencia = '${reference}'
          AND idCliente = ${idCliente}
      `)
      .then(async (result2) => {
        let odometer = await String(result.recordset[0].odometro)
        if (odometer.indexOf('.') == -1) {
          odometer = (odometer.substring(0, odometer.length - 3) + '.' + odometer.substring(odometer.length - 3, odometer.length))
        } else {
          odometer = odometer
        }
        let vKm = odometer.substring(0, odometer.indexOf('.'))
        let M = odometer.substring(odometer.indexOf('.') + 1, odometer.length)
        
        if(vKm.length > 0) {
          switch (vKm.length) {
            case 1:
              odometer = String(vKm) + ' Km'
            break
            case 2:
              odometer = String(vKm)
            break
            case 3:
              odometer = String(vKm) + ' Km'
            break
            case 4:
              odometer = String(vKm.substring(0, 1)) + '.' + String(vKm.substring(1, vKm.length)) + ' Km'
            break
            case 5:
              odometer = String(vKm.substring(0, 2)) + '.' + String(vKm.substring(2, vKm.length)) + ' Km'
            break
            case 6:
              odometer = String(vKm.substring(0, 3)) + '.' + String(vKm.substring(3, vKm.length)) + ' Km'
            break
          }
        } else {
          odometer = String(M) + ' m'
        }
        let objectTemp = {
          dataGPS: result.recordset[0].dataGPS,
          placa: result.recordset[0].placa,
          etiqueta: result.recordset[0].etiqueta,
          coords: {latitude: parseFloat(result.recordset[0].latitude), longitude: parseFloat(result.recordset[0].longitude)},
          localizacao: result.recordset[0].localizacao,
          velocidade: result.recordset[0].velocidade,
          ignicao: result.recordset[0].ignicao,
          statusVeic: result.recordset[0].statusVeic,
          evento: result.recordset[0].evento,
          odometro: odometer,
          tensao: result.recordset[0].tensao,
          temperatura: result.recordset[0].temperatura,
          referenciaCoords: result.recordset[0].PontoReferencia ? {latitude: parseFloat(result2.recordset[0].latitude), longitude: parseFloat(result2.recordset[0].longitude)} : null,
          referenciaNome: result.recordset[0].PontoReferencia ? result2.recordset[0].nomeReferencia : null,
          referenciaRaio: result.recordset[0].PontoReferencia ? result2.recordset[0].raio : null
        }
        arrayTemp.push(objectTemp)
      })
    } else {
      res.send(false)
    }
  })
  res.send(arrayTemp)
})

app.get('/get_pos_monitoring_suntech410', async (req, res) => {
  let arrayTemp = []
  await pool.request()
  .query(`
    SELECT TOP 1
      m.dataGPS,
      IIF ( m.online = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( m.online = 1, '#00EB00', '#FF0322' ) as statusVeic,
      m.latitude,
      m.longitude,
      m.localizacao,
      m.placa,
      m.battery AS tensao,
      m.velocidade,
      m.PontoReferencia,
      m.idCliente,
      m.temperatura,
      cv.etiqueta
    FROM movimento_suntech_frontend_410 m
      LEFT JOIN cadastro_veiculo cv ON cv.id = m.id
    WHERE m.placa='${req.query.place}'
    ORDER BY dataGPS DESC
  `)
  .then(async (result) => {
    if (result.recordset[0]) {
      let objectTemp = {
        dataGPS: result.recordset[0].dataGPS,
        placa: result.recordset[0].placa,
        etiqueta: result.recordset[0].etiqueta,
        coords: {latitude: parseFloat(result.recordset[0].latitude), longitude: parseFloat(result.recordset[0].longitude)},
        localizacao: result.recordset[0].localizacao,
        velocidade: result.recordset[0].velocidade,
        ignicao: result.recordset[0].ignicao,
        statusVeic: result.recordset[0].statusVeic,
        evento: result.recordset[0].evento,
        odometro: 0,
        tensao: result.recordset[0].tensao,
        temperatura: result.recordset[0].temperatura,
      }
      arrayTemp.push(objectTemp)
    } else {
      res.send(false)
    }
  })
  res.send(arrayTemp)
})

app.get('/get_pos_monitoring_j16', async (req, res) => {
  let arrayTemp = []
  await pool.request()
  .query(`
    SELECT TOP 1
      m.dataGPS,
      IIF ( m.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( m.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      m.latitude,
      m.longitude,
      m.endereco AS localizacao,
      m.placa,
      m.tensao_bateria_interna AS tensao,
      m.velocidade,
      m.PontoReferencia,
      m.id_cliente,
      cv.etiqueta
    FROM movimento_j16_frontend m
      LEFT JOIN cadastro_veiculo cv ON cv.serial = m.serial
    WHERE m.placa='${req.query.place}'
    ORDER BY dataGPS DESC
  `)
  .then(async (result) => {
    if (result.recordset[0]) {
      let objectTemp = {
        dataGPS: result.recordset[0].dataGPS,
        placa: result.recordset[0].placa,
        etiqueta: result.recordset[0].etiqueta,
        coords: {latitude: parseFloat(result.recordset[0].latitude), longitude: parseFloat(result.recordset[0].longitude)},
        localizacao: result.recordset[0].localizacao,
        velocidade: result.recordset[0].velocidade,
        ignicao: result.recordset[0].ignicao,
        statusVeic: result.recordset[0].statusVeic,
        evento: result.recordset[0].evento,
        odometro: 0,
        tensao: result.recordset[0].tensao,
      }
      arrayTemp.push(objectTemp)
    } else {
      res.send(false)
    }
  })
  res.send(arrayTemp)
})

// --------------------------------------------------------- POSITION
app.get('/get_pos_trac', async (req, res) => {
  let array = []
  let idDoUsuario = req.query.idUser
  let perfilVisualizacao = await GetPermissaoVisualizacao(idDoUsuario)

  if (perfilVisualizacao == 3) {
  await pool.request()
  .query(`
    SELECT DISTINCT
      mm.serial as idVeiculo, 
      mm.tensao, 
      mm.placa, 
      g.descricao as evento, 
      cast(mm.velocidade as numeric(10, 0)) as velocidade,
      mm.longitude, 
      mm.latitude, 
      mm.localizacao, 
      mm.dataServidor,
      mm.dataGPS,
      mm.hodometro, 
      cv.etiqueta,
      mm.idCliente,
      mm.horimetro,
      IIF ( mm.alertaIgnicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( mm.alertaIgnicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      m.nome AS motorista,
      cv.idCategoria as categoria,
      asu.id_usuario
    FROM movimento_grcad_frontend mm
      LEFT JOIN evento_grcad g ON mm.evento = g.id 
      LEFT JOIN cadastro_veiculo cv ON cv.serial = mm.serial
      LEFT JOIN cliente_cadastro cli ON cli.id = mm.idCliente
      LEFT JOIN assoc_veiculos_usuarios asu on asu.id_veiculo = cv.id
      LEFT JOIN motorista m ON m.rfid = mm.gsRfid
    WHERE asu.id_usuario = ${req.query.idUser}
      AND mm.placa is not null 
      AND dataServidor = (
        SELECT MAX(dataServidor) FROM movimento_grcad_frontend i
        WHERE mm.serial = i.serial 
          AND mm.idCliente = i.idCliente
          AND evento is not null
      ) 
    GROUP BY mm.serial, mm.alertaIgnicao, mm.tensao, mm.placa, g.descricao, mm.velocidade, mm.longitude, mm.latitude, mm.localizacao, mm.dataServidor, mm.dataGPS, mm.hodometro, cv.etiqueta, mm.idCliente, mm.horimetro,cv.idcategoria, asu.id_usuario, m.nome
  `)
  .then( async (res1) => {
    
    let arrayTemp = res1.recordset
    for(let i = 0;i < arrayTemp.length; i++) {
      await pool.request().query(`
        SELECT idCliente FROM assoc_veiculo WHERE placa='${arrayTemp[i].placa}'
      `)
      .then( async (res2) => {

        if (res2.rowsAffected > 0) {
          await pool.request()
          .query(`
            SELECT
              ativo
            FROM cliente_cadastro
            WHERE
              id='${res2.recordset[0].idCliente}'
              AND ativo = 1
          `)
          .then((res3) => {
            if (res3.rowsAffected > 0) {
              array.push(arrayTemp[i])
            }
          })
          .catch(err => console.log(err))
        } else {
          await pool.request().query(`
            SELECT ativo FROM cliente_cadastro WHERE id='${arrayTemp[i].idCliente}' and ativo=1
          `)
          .then( async (res2) => {
            if (res2.rowsAffected > 0) {
              array.push(arrayTemp[i])
            }
          })
        }

      })
    }

  })
  res.send(array)
} else {
  let clientsTemp = (req.query.id).split(',')
  let clients = []

  for (let y = 0; y < clientsTemp.length; y++) {
    await pool.request().query(`
      SELECT ativo FROM cliente_cadastro WHERE id='${clientsTemp[y]}' AND ativo=1
    `)
    .then( async (result) => {  
      if (result.rowsAffected > 0) {
        clients.push(clientsTemp[y])
      }
    })
  }

  if (clients.length > 0) {
    await pool.request()
    .query(`
      SELECT DISTINCT
        mm.serial as idVeiculo, 
        mm.tensao, 
        mm.placa, 
        g.descricao as evento, 
        cast(mm.velocidade as numeric(10, 0)) as velocidade,
        mm.longitude, 
        mm.latitude, 
        mm.localizacao, 
        mm.dataServidor,
        mm.dataGPS,
        mm.hodometro, 
        cv.etiqueta,
        mm.idCliente,
        mm.horimetro,
        IIF ( mm.alertaIgnicao = 1, 'Ligado', 'Desligado') as ignicao,
        IIF ( mm.alertaIgnicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
        m.nome AS motorista,
        cv.idCategoria as categoria,
        asu.id_usuario
      FROM movimento_grcad_frontend mm
        LEFT JOIN evento_grcad g ON mm.evento = g.id 
        LEFT JOIN cadastro_veiculo cv ON cv.serial = mm.serial
        LEFT JOIN cliente_cadastro cli ON cli.id = mm.idCliente
        LEFT JOIN assoc_veiculos_usuarios asu on asu.id_veiculo = cv.id
        LEFT JOIN motorista m ON m.rfid = mm.gsRfid
      WHERE mm.idCliente in (${clients.toString().split(',')})
        AND mm.placa is not null 
        AND dataServidor = (
          SELECT MAX(dataServidor) FROM movimento_grcad_frontend i
            WHERE mm.serial = i.serial 
              AND mm.idCliente = i.idCliente
              AND evento is not null
        ) 
      GROUP BY mm.serial, mm.alertaIgnicao, mm.tensao, mm.placa, g.descricao, mm.velocidade, mm.longitude, mm.latitude, mm.localizacao, mm.dataServidor, mm.dataGPS, mm.hodometro, cv.etiqueta, mm.idCliente, mm.horimetro,cv.idcategoria, asu.id_usuario, m.nome
    `)
    .then( async (res1) => {
      let arrayTemp = res1.recordset
      for(let i = 0; i < arrayTemp.length; i++) {
        array.push(arrayTemp[i])
      }
    })
  
    res.send(array)
  } else {
    res.send(null)
  }
}
});

app.get('/get_pos_teltonika', async (req, res) => {
  let array = []
  let idDoUsuario = req.query.idUser;
  let perfilVisualizacao = await GetPermissaoVisualizacao(idDoUsuario);

  if (perfilVisualizacao == 3) {
  await pool.request()
  .query(`
    SELECT * FROM
      (SELECT DISTINCT
        mm.serial as idVeiculo,
        mm.idCliente,
        mm.placa,
        mm.dataServidor,
        mm.latitude, 
        mm.longitude,
        mm.endereco,
        mm.velocidade2,
        IIF ( mm.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
        IIF ( mm.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
        mm.temperaturaS2,
        mm.umidadeS2,
        mm.bateria_externa,
        cv.idUsuario,
        cv.etiqueta,
        ROW_NUMBER() OVER (PARTITION BY mm.serial ORDER BY dataServidor DESC ) AS RowNumber
      FROM movimento_teltonika_frontend mm
        LEFT JOIN cadastro_veiculo cv ON cv.serial = mm.serial
        LEFT JOIN assoc_veiculos_usuarios asu ON asu.id_veiculo = cv.id
      WHERE asu.id_usuario = ${req.query.idUser}
        AND cv.serial = mm.serial
        AND asu.id_veiculo = cv.id
      ) AS a
    WHERE a.RowNumber = 1
  `)
  .then( async (res1) => {

    let arrayTemp = res1.recordset
    for(let i = 0;i < arrayTemp.length; i++) {
      await pool.request().query(`
        SELECT idCliente FROM assoc_veiculo WHERE placa='${arrayTemp[i].placa}'
      `)
      .then( async (res2) => {

        if (res2.rowsAffected > 0) {
          await pool.request()
          .query(`
            SELECT
              ativo
            FROM cliente_cadastro
            WHERE
              id='${res2.recordset[0].idCliente}'
              AND ativo = 1
          `)
          .then((res3) => {
            if (res3.rowsAffected > 0) {
              array.push(arrayTemp[i])
            }
          })
          .catch(err => console.log(err))
        } else {
          await pool.request().query(`
            SELECT ativo FROM cliente_cadastro WHERE id='${arrayTemp[i].idCliente}' and ativo=1
          `)
          .then( async (res2) => {
            if (res2.rowsAffected > 0) {
              array.push(arrayTemp[i])
            }
          })
        }

      })
    }

  })
  res.send(array)
} else {
  let clientsTemp = (req.query.id).split(',')
  let clients = []

  for (let y = 0; y < clientsTemp.length; y++) {
    await pool.request().query(`
      SELECT ativo FROM cliente_cadastro WHERE id='${clientsTemp[y]}' AND ativo=1
    `)
    .then( async (result) => {  
      if (result.rowsAffected > 0) {
        clients.push(clientsTemp[y])
      }
    })
    .catch(err => console.log(err))
  }
  
  if (clients.length > 0) {
  await pool.request()
  .query(`
    SELECT * FROM
      (SELECT DISTINCT
        mm.serial as idVeiculo,
        mm.idCliente,
        mm.placa,
        mm.dataServidor,
        mm.latitude, 
        mm.longitude,
        mm.endereco,
        mm.velocidade2,
        IIF ( mm.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
        IIF ( mm.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
        mm.temperaturaS2,
        mm.umidadeS2,
        mm.bateria_externa,
        cv.idUsuario,
        cv.etiqueta,
        ROW_NUMBER() OVER (PARTITION BY mm.serial ORDER BY dataServidor DESC ) AS RowNumber
      FROM movimento_teltonika_frontend mm
        LEFT JOIN cadastro_veiculo cv ON cv.serial = mm.serial
        LEFT JOIN assoc_veiculos_usuarios asu ON asu.id_veiculo = cv.id
      WHERE mm.idCliente in (${clients.toString().split(',')})
        AND cv.serial = mm.serial
        AND asu.id_veiculo = cv.id
      ) AS a
    WHERE a.RowNumber = 1
  `)
  .then( async (res1) => {
    let arrayTemp = res1.recordset
    for(let i = 0; i < arrayTemp.length; i++) {
      array.push(arrayTemp[i])
    }
  })
  
  res.send(array)
  } else {
    res.send(null)
  }
}
});

app.get('/get_pos_suntech', async (req, res) => {
  let array = []
  let idDoUsuario = req.query.idUser;
  let perfilVisualizacao = await GetPermissaoVisualizacao(idDoUsuario);

  if (perfilVisualizacao == 3) {
  await pool.request()
  .query(`
    SELECT * FROM
      (SELECT DISTINCT
        mm.idVeiculo,
        mm.tecnologia,
        mm.placa,
        mm.dataGPS,
        mm.latitude, 
        mm.longitude,
        mm.localizacao,
        mm.idCliente,
			  CAST(CAST(mm.velocidade AS FLOAT) AS INT) as velocidade,
        IIF (mm.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
			  IIF (mm.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
        mm.tensao,
        mm.odometro,
        mm.horimetro,
        mm.temperaturaS1,
        cv.etiqueta,
			  m.nome AS motorista,
        ROW_NUMBER() OVER (PARTITION BY idVeiculo ORDER BY dataGPS DESC ) AS RowNumber
      FROM movimento_suntech_frontend mm
        LEFT JOIN cadastro_veiculo cv ON cv.serial = mm.idVeiculo
        LEFT JOIN assoc_veiculos_usuarios asu ON asu.id_veiculo = cv.id
        LEFT JOIN motorista m ON m.rfid = mm.gsRfid
      WHERE asu.id_usuario = ${req.query.idUser}
      ) AS a
    WHERE a.RowNumber = 1
  `)
  .then( async (res1) => {
    
    let arrayTemp = res1.recordset
    for(let i = 0;i < arrayTemp.length; i++) {
      await pool.request().query(`
        SELECT idCliente FROM assoc_veiculo WHERE placa='${arrayTemp[i].placa}'
      `)
      .then( async (res2) => {

        if (res2.rowsAffected > 0) {
          await pool.request()
          .query(`
            SELECT
              ativo
            FROM cliente_cadastro
            WHERE
              id='${res2.recordset[0].idCliente}'
              AND ativo = 1
          `)
          .then((res3) => {
            if (res3.rowsAffected > 0) {
              array.push(arrayTemp[i])
            }
          })
          .catch(err => console.log(err))
        } else {
          await pool.request().query(`
            SELECT ativo FROM cliente_cadastro WHERE id='${arrayTemp[i].idCliente}' and ativo=1
          `)
          .then( async (res2) => {
            if (res2.rowsAffected > 0) {
              array.push(arrayTemp[i])
            }
          })
        }

      })
    }

  })
  res.send(array)

} else {
  let clientsTemp = (req.query.id).split(',')
  let clients = []

  for (let y = 0; y < clientsTemp.length; y++) {
    await pool.request().query(`
      SELECT ativo FROM cliente_cadastro WHERE id='${clientsTemp[y]}' AND ativo=1
    `)
    .then( async (result) => {  
      if (result.rowsAffected > 0) {
        clients.push(clientsTemp[y])
      }
    })
    .catch(err => console.log(err))
  }
  
  if (clients.length > 0) {
  await pool.request()
  .query(`
    SELECT * FROM
      (SELECT DISTINCT
        mm.idVeiculo,
        mm.tecnologia,
        mm.placa,
        mm.dataGPS,
        mm.latitude, 
        mm.longitude,
        mm.localizacao,
        mm.idCliente,
			  CAST(CAST(mm.velocidade AS FLOAT) AS INT) as velocidade,
        IIF (mm.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
			  IIF (mm.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
        mm.tensao,
        mm.odometro,
        mm.horimetro,
        mm.temperaturaS1,
        cv.etiqueta,
			  m.nome AS motorista,
        ROW_NUMBER() OVER (PARTITION BY idVeiculo ORDER BY dataGPS DESC ) AS RowNumber
      FROM movimento_suntech_frontend mm
        LEFT JOIN cadastro_veiculo cv ON cv.serial = mm.idVeiculo
        LEFT JOIN assoc_veiculos_usuarios asu ON asu.id_veiculo = cv.id
        LEFT JOIN motorista m ON m.rfid = mm.gsRfid
      WHERE mm.idCliente in (${clients.toString().split(',')})
      ) AS a
    WHERE a.RowNumber = 1
  `)
  .then( async (res1) => {
    let arrayTemp = res1.recordset
    for(let i = 0; i < arrayTemp.length; i++) {
      array.push(arrayTemp[i])
    }
  })
  res.send(array)
  } else {
    res.send(null)
  }
}
});

app.get('/get_pos_gv57', async (req, res) => {
  let array = []
  let idDoUsuario = req.query.idUser;
  let perfilVisualizacao = await GetPermissaoVisualizacao(idDoUsuario);

  if (perfilVisualizacao == 3) {

  await pool.request()
  .query(`
    SELECT * FROM    
      (SELECT DISTINCT
          mm.uniqueid as idVeiculo,
          mm.idCliente,
          mm.event as tecnologia,
          mm.placa,
          FORMAT( DATEADD (HOUR, CAST(0 AS INTEGER), mm.utctime), 'yyyy-MM-dd HH:mm:ss' ) AS dataGPS,
          mm.latitude,
          mm.longitude,
          mm.localizacao,
          mm.speed as velocidade,
          mm.externalpower,
          IIF ( mm.ignicao = 0, 'Ligado', 'Desligado') as ignicao,
          IIF ( mm.ignicao = 0, '#00EB00', '#FF0322' ) as statusVeic,
          m.nome AS motorista,
          cv.idUsuario,
          cv.etiqueta,
          ROW_NUMBER() OVER (PARTITION BY uniqueid ORDER BY mm.utctime DESC ) AS RowNumber
        FROM movimento_gv57_frontend mm
          LEFT JOIN cadastro_veiculo cv ON cv.placa = mm.placa
          LEFT JOIN assoc_veiculos_usuarios asu ON asu.id_veiculo = cv.id
      LEFT JOIN motorista m ON m.rfid = mm.gsRfid
        WHERE asu.id_usuario = ${req.query.idUser}
      ) AS a
    WHERE a.RowNumber = 1
  `)
  .then( async (res1) => {
    
    let arrayTemp = res1.recordset
    for(let i = 0;i < arrayTemp.length; i++) {
      await pool.request().query(`
        SELECT idCliente FROM assoc_veiculo WHERE placa='${arrayTemp[i].placa}'
      `)
      .then( async (res2) => {

        if (res2.rowsAffected > 0) {
          await pool.request()
          .query(`
            SELECT
              ativo
            FROM cliente_cadastro
            WHERE
              id='${res2.recordset[0].idCliente}'
              AND ativo = 1
          `)
          .then((res3) => {
            if (res3.rowsAffected > 0) {
              array.push(arrayTemp[i])
            }
          })
          .catch(err => console.log(err))
        } else {
          await pool.request().query(`
            SELECT ativo FROM cliente_cadastro WHERE id='${arrayTemp[i].idCliente}' and ativo=1
          `)
          .then( async (res2) => {
            if (res2.rowsAffected > 0) {
              array.push(arrayTemp[i])
            }
          })
        }

      })
    }
  })
  res.send(array)
} else {
  let clientsTemp = (req.query.id).split(',')
  let clients = []

  for (let y = 0; y < clientsTemp.length; y++) {
    await pool.request().query(`
      SELECT ativo FROM cliente_cadastro WHERE id='${clientsTemp[y]}' AND ativo=1
    `)
    .then( async (result) => {  
      if (result.rowsAffected > 0) {
        clients.push(clientsTemp[y])
      }
    })
    .catch(err => console.log(err))
  }

  if (clients.length > 0) {
    await pool.request()
    .query(`
      SELECT * FROM    
        (SELECT DISTINCT
            mm.uniqueid as idVeiculo,
            mm.idCliente,
            mm.event as tecnologia,
            mm.placa,
            FORMAT( DATEADD (HOUR, CAST(0 AS INTEGER), mm.utctime), 'yyyy-MM-dd HH:mm:ss' ) AS dataGPS,
            mm.latitude,
            mm.longitude,
            mm.localizacao,
            mm.speed as velocidade,
            mm.externalpower,
            IIF ( mm.ignicao = 0, 'Ligado', 'Desligado') as ignicao,
            IIF ( mm.ignicao = 0, '#00EB00', '#FF0322' ) as statusVeic,
            m.nome AS motorista,
            cv.idUsuario,
            cv.etiqueta,
            ROW_NUMBER() OVER (PARTITION BY uniqueid ORDER BY mm.utctime DESC ) AS RowNumber
          FROM movimento_gv57_frontend mm
            LEFT JOIN cadastro_veiculo cv ON cv.placa = mm.placa
            LEFT JOIN assoc_veiculos_usuarios asu ON asu.id_veiculo = cv.id
            LEFT JOIN motorista m ON m.rfid = mm.gsRfid
          WHERE mm.idCliente in (${clients.toString().split(',')})
        ) AS a
      WHERE a.RowNumber = 1
    `)
    .then( async (res1) => {
      let arrayTemp = res1.recordset
      for(let i = 0; i < arrayTemp.length; i++) {
        array.push(arrayTemp[i])
      }
    })
    res.send(array)
  } else {
    res.send(null)
  }
  }
});

app.get('/get_pos_gv75', async (req, res) => {
  let array = []
  let idDoUsuario = req.query.idUser;
  let perfilVisualizacao = await GetPermissaoVisualizacao(idDoUsuario);

  if (perfilVisualizacao == 3) {
    await pool.request()
    .query(`
      SELECT * FROM    
        (SELECT DISTINCT
            mm.uniqueid as idVeiculo,
            mm.idCliente,
            mm.event as tecnologia,
            mm.placa,
            FORMAT( DATEADD (HOUR, CAST(- 3 AS INTEGER), mm.utctime), 'yyyy-MM-dd HH:mm:ss' ) AS dataGPS,
            mm.latitude,
            mm.longitude,
            mm.localizacao,
            mm.speed as velocidade,
            mm.externalpower,
            IIF ( mm.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
            IIF ( mm.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
            cv.idUsuario,
            cv.etiqueta,
            ROW_NUMBER() OVER (PARTITION BY uniqueid ORDER BY mm.utctime DESC ) AS RowNumber
          FROM movimento_gv75_frontend mm
            LEFT JOIN cadastro_veiculo cv ON cv.placa = mm.placa
            LEFT JOIN assoc_veiculos_usuarios asu ON asu.id_veiculo = cv.id
          WHERE asu.id_usuario = ${req.query.idUser}
        ) AS a
      WHERE a.RowNumber = 1
    `)
    .then( async (res1) => {
      
      let arrayTemp = res1.recordset
      for(let i = 0;i < arrayTemp.length; i++) {
        await pool.request().query(`
          SELECT idCliente FROM assoc_veiculo WHERE placa='${arrayTemp[i].placa}'
        `)
        .then( async (res2) => {
  
          if (res2.rowsAffected > 0) {
            await pool.request()
            .query(`
              SELECT
                ativo
              FROM cliente_cadastro
              WHERE
                id='${res2.recordset[0].idCliente}'
                AND ativo = 1
            `)
            .then((res3) => {
              if (res3.rowsAffected > 0) {
                array.push(arrayTemp[i])
              }
            })
            .catch(err => console.log(err))
          } else {
            await pool.request().query(`
              SELECT ativo FROM cliente_cadastro WHERE id='${arrayTemp[i].idCliente}' and ativo=1
            `)
            .then( async (res2) => {
              if (res2.rowsAffected > 0) {
                array.push(arrayTemp[i])
              }
            })
          }
  
        })
      }
    })
    res.send(array)
  } else {
    let clientsTemp = (req.query.id).split(',')
    let clients = []

    for (let y = 0; y < clientsTemp.length; y++) {
      await pool.request().query(`
        SELECT ativo FROM cliente_cadastro WHERE id='${clientsTemp[y]}' AND ativo=1
      `)
      .then( async (result) => {  
        if (result.rowsAffected > 0) {
          clients.push(clientsTemp[y])
        }
      })
      .catch(err => console.log(err))
    }
  
    if (clients.length > 0) {
    await pool.request()
    .query(`
        SELECT * FROM    
        (SELECT DISTINCT
            mm.uniqueid as idVeiculo,
            mm.idCliente,
            mm.event as tecnologia,
            mm.placa,
            FORMAT( DATEADD (HOUR, CAST(- 3 AS INTEGER), mm.utctime), 'yyyy-MM-dd HH:mm:ss' ) AS dataGPS,
            mm.latitude,
            mm.longitude,
            mm.localizacao,
            mm.speed as velocidade,
            mm.externalpower,
            IIF ( mm.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
            IIF ( mm.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
            cv.idUsuario,
            cv.etiqueta,
            ROW_NUMBER() OVER (PARTITION BY uniqueid ORDER BY mm.utctime DESC ) AS RowNumber
          FROM movimento_gv75_frontend mm
            LEFT JOIN cadastro_veiculo cv ON cv.placa = mm.placa
            LEFT JOIN assoc_veiculos_usuarios asu ON asu.id_veiculo = cv.id
          WHERE mm.idCliente in (${clients.toString().split(',')})
        ) AS a
      WHERE a.RowNumber = 1
      `)
      .then( async (res1) => {
        let arrayTemp = res1.recordset
        for(let i = 0; i < arrayTemp.length; i++) {
          array.push(arrayTemp[i])
        }
      })
      res.send(array)
    } else {
      res.send(null)
    }
  }
});

app.get('/get_pos_vl01', async (req, res) => {
  let array = []
  let idDoUsuario = req.query.idUser;
  let perfilVisualizacao = await GetPermissaoVisualizacao(req.query.idUser);

  if (perfilVisualizacao == 3) {
    await pool.request()
    .query(`
      SELECT * FROM
        (SELECT DISTINCT
          m.serial AS idVeiculo,
          m.placa,
          m.id_cliente AS idCliente,
          m.dataGPS,
          CAST (m.latitude AS FLOAT) * 1 AS latitude,
          CAST (m.longitude AS FLOAT) * 1 AS longitude,
          m.endereco,
          m.velocidade,
          m.tensao_bateria_externa,
          IIF ( m.ignicao = 1, 'Ligado', 'Desligado') AS ignicao,
          IIF ( m.ignicao = 1, '#00EB00', '#FF0322' ) AS statusVeic,
          m.odometro,
          cv.idUsuario,
          cv.etiqueta,
          ROW_NUMBER() OVER (PARTITION BY m.serial ORDER BY m.dataGPS DESC ) AS RowNumber
          FROM movimento_vl01_frontend m
            LEFT JOIN cadastro_veiculo cv ON cv.serial = m.serial
            LEFT JOIN assoc_veiculos_usuarios asu ON asu.id_veiculo = cv.id
          WHERE asu.id_usuario = '${idDoUsuario}'
        ) AS a
      WHERE a.RowNumber = 1
    `)
    .then( async (res1) => {
    
      let arrayTemp = res1.recordset
      for(let i = 0;i < arrayTemp.length; i++) {
        await pool.request().query(`
          SELECT idCliente FROM assoc_veiculo WHERE placa='${arrayTemp[i].placa}'
        `)
        .then( async (res2) => {
  
          if (res2.rowsAffected > 0) {
            await pool.request()
            .query(`
              SELECT
                ativo
              FROM cliente_cadastro
              WHERE
                id='${res2.recordset[0].idCliente}'
                AND ativo = 1
            `)
            .then((res3) => {
              if (res3.rowsAffected > 0) {
                array.push(arrayTemp[i])
              }
            })
            .catch(err => console.log(err))
          } else {
            await pool.request().query(`
              SELECT ativo FROM cliente_cadastro WHERE id='${arrayTemp[i].idCliente}' and ativo=1
            `)
            .then( async (res2) => {
              if (res2.rowsAffected > 0) {
                array.push(arrayTemp[i])
              }
            })
          }
  
        })
      }

    })
    res.send(array)
  } else {
    let clientsTemp = (req.query.id).split(',')
    let clients = []

    for (let y = 0; y < clientsTemp.length; y++) {
      await pool.request().query(`
        SELECT ativo FROM cliente_cadastro WHERE id='${clientsTemp[y]}' AND ativo=1
      `)
      .then( async (result) => {  
        if (result.rowsAffected > 0) {
          clients.push(clientsTemp[y])
        }
      })
      .catch(err => console.log(err))
    }

    if (clients.length > 0) {
      await pool.request()
      .query(`
        SELECT * FROM
          (SELECT DISTINCT
            m.serial AS idVeiculo,
            m.placa,
            m.id_cliente,
            m.dataGPS,
            CAST (m.latitude AS FLOAT) * 1 AS latitude,
            CAST (m.longitude AS FLOAT) * 1 AS longitude,
            m.endereco,
            m.velocidade,
            m.tensao_bateria_externa,
            IIF ( m.ignicao = 1, 'Ligado', 'Desligado') AS ignicao,
            IIF ( m.ignicao = 1, '#00EB00', '#FF0322' ) AS statusVeic,
            m.odometro,
            cv.idUsuario,
            cv.etiqueta,
            ROW_NUMBER() OVER (PARTITION BY m.serial ORDER BY m.dataGPS DESC ) AS RowNumber
          FROM movimento_vl01_frontend m, cadastro_veiculo cv
          WHERE m.id_cliente in (${clients.toString().split(',')})
            AND cv.serial = m.serial) AS a
        WHERE a.RowNumber = 1
      `)
      .then( async (res1) => {
        let arrayTemp = res1.recordset
        for(let i = 0; i < arrayTemp.length; i++) {
          array.push(arrayTemp[i])
        }
      })
      res.send(array)
    } else {
      res.send(null)
    }
  }
});

app.get('/get_pos_vl02', async (req, res) => {
  let array = []
  let idDoUsuario = req.query.idUser;
  let perfilVisualizacao = await GetPermissaoVisualizacao(idDoUsuario);

  if (perfilVisualizacao == 3) {
    await pool.request()
    .query(`
      SELECT * FROM
      (SELECT DISTINCT
          m.serial AS idVeiculo,
          m.placa,
          m.id_cliente AS idCliente,
          m.dataGPS,
          CAST (m.latitude AS FLOAT) * 1 AS latitude,
          CAST (m.longitude AS FLOAT) * 1 AS longitude,
          m.endereco,
          m.velocidade,
          m.tensao_bateria_externa,
          IIF ( m.ignicao = 1, 'Ligado', 'Desligado') AS ignicao,
          IIF ( m.ignicao = 1, '#00EB00', '#FF0322' ) AS statusVeic,
          m.odometro,
          m.temperaturaS1,
          mm.nome AS motorista,
          cv.idUsuario,
          cv.etiqueta,
          ROW_NUMBER() OVER (PARTITION BY m.serial ORDER BY m.dataGPS DESC ) AS RowNumber
          FROM movimento_vl02_frontend m
            LEFT JOIN cadastro_veiculo cv ON cv.serial = m.serial
            LEFT JOIN assoc_veiculos_usuarios asu ON asu.id_veiculo = cv.id
          LEFT JOIN motorista mm ON mm.rfid = m.rfid
          WHERE asu.id_usuario = '${idDoUsuario}'
        ) AS a
      WHERE a.RowNumber = 1
    `)
    .then( async (res1) => {
    
      let arrayTemp = res1.recordset
      for(let i = 0;i < arrayTemp.length; i++) {
        await pool.request().query(`
          SELECT idCliente FROM assoc_veiculo WHERE placa='${arrayTemp[i].placa}'
        `)
        .then( async (res2) => {
  
          if (res2.rowsAffected > 0) {
            await pool.request()
            .query(`
              SELECT
                ativo
              FROM cliente_cadastro
              WHERE
                id='${res2.recordset[0].idCliente}'
                AND ativo = 1
            `)
            .then((res3) => {
              if (res3.rowsAffected > 0) {
                array.push(arrayTemp[i])
              }
            })
            .catch(err => console.log(err))
          } else {
            await pool.request().query(`
              SELECT ativo FROM cliente_cadastro WHERE id='${arrayTemp[i].idCliente}' and ativo=1
            `)
            .then( async (res2) => {
              if (res2.rowsAffected > 0) {
                array.push(arrayTemp[i])
              }
            })
          }
  
        })
      }

    })
    res.send(array)
  } else {
    let clientsTemp = (req.query.id).split(',')
    let clients = []

    for (let y = 0; y < clientsTemp.length; y++) {
      await pool.request().query(`
        SELECT ativo FROM cliente_cadastro WHERE id='${clientsTemp[y]}' AND ativo=1
      `)
      .then( async (result) => {  
        if (result.rowsAffected > 0) {
          clients.push(clientsTemp[y])
        }
      })
      .catch(err => console.log(err))
    }
    
    if (clients.length > 0) {
      await pool.request()
      .query(`
      SELECT * FROM
          (SELECT DISTINCT
            m.serial AS idVeiculo,
            m.placa,
            m.id_cliente,
            m.dataGPS,
            CAST (m.latitude AS FLOAT) * 1 AS latitude,
            CAST (m.longitude AS FLOAT) * 1 AS longitude,
            m.endereco,
            m.velocidade,
            m.tensao_bateria_externa,
            IIF ( m.ignicao = 1, 'Ligado', 'Desligado') AS ignicao,
            IIF ( m.ignicao = 1, '#00EB00', '#FF0322' ) AS statusVeic,
            m.odometro,
            m.temperaturaS1,
            mm.nome AS motorista,
            cv.idUsuario,
            cv.etiqueta,
            ROW_NUMBER() OVER (PARTITION BY m.serial ORDER BY m.dataGPS DESC ) AS RowNumber
          FROM movimento_vl02_frontend m
              LEFT JOIN cadastro_veiculo cv ON cv.serial = m.serial
              LEFT JOIN assoc_veiculos_usuarios asu ON asu.id_veiculo = cv.id
            LEFT JOIN motorista mm ON mm.rfid = m.rfid
          WHERE m.id_cliente in (${clients.toString().split(',')})
            AND cv.serial = m.serial) AS a
        WHERE a.RowNumber = 1
      `)
      .then( async (res1) => {
        let arrayTemp = res1.recordset
        for(let i = 0; i < arrayTemp.length; i++) {
          array.push(arrayTemp[i])
        }
      })
      res.send(array)
    } else {
      res.send(null)
    }
  }
});

app.get('/get_pos_vl03', async (req, res) => {
  let array = []
  let idDoUsuario = req.query.idUser;
  let perfilVisualizacao = await GetPermissaoVisualizacao(idDoUsuario);

  if (perfilVisualizacao == 3) {
    await pool.request()
    .query(`
      SELECT * FROM
        (SELECT DISTINCT
            ms.serial AS idVeiculo,
            ms.id_cliente,
            ms.placa,
            ms.dataGPS,
            ms.dataServidor,
            CAST (ms.latitude AS FLOAT) * -1 AS latitude,
            CAST (ms.longitude AS FLOAT) * -1 AS longitude,
            ms.endereco,
            ms.velocidade,
            ms.tensao_bateria_externa,
            IIF ( ms.ignicao = 1, 'Ligado', 'Desligado') AS ignicao,
            IIF ( ms.ignicao = 1, '#00EB00', '#FF0322' ) AS statusVeic,
            cv.idUsuario,
            cv.etiqueta,
            ROW_NUMBER() OVER (PARTITION BY ms.serial ORDER BY ms.dataGPS DESC ) AS RowNumber
          FROM movimento_vl03_frontend ms, cadastro_veiculo cv, assoc_veiculos_usuarios asu
          WHERE asu.id_usuario = '${idDoUsuario}'
            AND cv.serial = ms.serial
            AND asu.id_veiculo = cv.id
        ) AS a
      WHERE a.RowNumber = 1
    `)
    .then( async (res1) => {
    
      let arrayTemp = res1.recordset
      for(let i = 0;i < arrayTemp.length; i++) {
        await pool.request().query(`
          SELECT idCliente FROM assoc_veiculo WHERE placa='${arrayTemp[i].placa}'
        `)
        .then( async (res2) => {
  
          if (res2.rowsAffected > 0) {
            await pool.request()
            .query(`
              SELECT
                ativo
              FROM cliente_cadastro
              WHERE
                id='${res2.recordset[0].idCliente}'
                AND ativo = 1
            `)
            .then((res3) => {
              if (res3.rowsAffected > 0) {
                array.push(arrayTemp[i])
              }
            })
            .catch(err => console.log(err))
          } else {
            await pool.request().query(`
              SELECT ativo FROM cliente_cadastro WHERE id='${arrayTemp[i].idCliente}' and ativo=1
            `)
            .then( async (res2) => {
              if (res2.rowsAffected > 0) {
                array.push(arrayTemp[i])
              }
            })
          }
  
        })
      }
    })
    res.send(array)
  } else {
    let clientsTemp = (req.query.id).split(',')
    let clients = []

    for (let y = 0; y < clientsTemp.length; y++) {
      await pool.request().query(`
        SELECT ativo FROM cliente_cadastro WHERE id='${clientsTemp[y]}' AND ativo=1
      `)
      .then( async (result) => {  
        if (result.rowsAffected > 0) {
          clients.push(clientsTemp[y])
        }
      })
      .catch(err => console.log(err))
    }

    if (clients.length > 0) {
      await pool.request()
      .query(`
        SELECT * FROM
          (SELECT DISTINCT
            x.serial as idVeiculo,
            x.placa,
            x.id_cliente,
            CAST (x.latitude AS FLOAT) * -1 AS latitude,
            CAST (x.longitude AS FLOAT) * -1 AS longitude,
            IIF ( x.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
            IIF ( x.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
            x.dataGPS,
            x.velocidade,
            x.endereco,
            x.tensao_bateria_externa,
            v.etiqueta,
            v.idUsuario,
            ROW_NUMBER() OVER (PARTITION BY x.serial ORDER BY x.dataGPS DESC ) AS RowNumber
          FROM movimento_vl03_frontend x, cadastro_veiculo v
          WHERE x.id_cliente in (${clients.toString().split(',')})
            AND v.serial = x.serial) AS a
        WHERE a.RowNumber = 1
      `)
      .then( async (res1) => {
      let arrayTemp = res1.recordset
      for(let i = 0; i < arrayTemp.length; i++) {
        array.push(arrayTemp[i])
      }
      })
      res.send(array)
    } else {
      res.send(null)
    }
  }
});

app.get('/get_pos_mt2000', async (req, res) => {
  let array = []
  let idDoUsuario = req.query.idUser;
  let perfilVisualizacao = await GetPermissaoVisualizacao(idDoUsuario);

  if (perfilVisualizacao == 3) {
    await pool.request()
    .query(`
      SELECT DISTINCT
      x.serial AS idVeiculo,
      x.idCliente,
      x.localizacao,
      x.placa,
      x.dataGPS,
      x.latitude,
      x.longitude,
      x.velocidade,
      x.tensao,
			IIF ( x.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
			IIF ( x.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      cv.idcategoria,
      cv.idUsuario,
      cv.idCategoria as categoria,
      cv.etiqueta
    FROM
      movimento_mt2000_frontend X
      JOIN alerts_Mt2000 amt
        ON x.MSG_TYPE = header_message
      JOIN cadastro_veiculo cv
        ON cv.placa = x.placa
      JOIN cliente_cadastro c
        ON c.id = x.idCliente
      LEFT JOIN ponto_referencia pr
        ON pr.id = x.idPontoReferencia
      LEFT JOIN area_restrita ar
        ON ar.id = x.idarearestrita
      JOIN assoc_veiculos_usuarios asu
        ON asu.id_veiculo = cv.id
    WHERE asu.id_usuario = ${idDoUsuario}
      AND dataGPS =
      (SELECT
        MAX(dataGPS)
      FROM
        movimento_mt2000_frontend i
      WHERE x.SERIAL = i.SERIAL
        AND i.LATITUDE IS NOT NULL
        AND i.LONGITUDE IS NOT NULL
        AND i.LATITUDE != ''
        AND i.LONGITUDE != '')
      AND x.LATITUDE IS NOT NULL
      AND x.LONGITUDE IS NOT NULL
      AND x.LATITUDE != ''
      AND x.LONGITUDE != ''
      GROUP BY x.serial, x.idCliente, x.localizacao, x.placa, x.dataGPS, x.latitude, x.longitude, x.velocidade, x.tensao, x.ignicao, cv.idcategoria, cv.idUsuario, cv.etiqueta
    `)
    .then( async (res1) => {
      let arrayTemp = res1.recordset
      for(let i = 0;i < arrayTemp.length; i++) {
        await pool.request().query(`
          SELECT idCliente FROM assoc_veiculo WHERE placa='${arrayTemp[i].placa}'
        `)
        .then( async (res2) => {
  
          if (res2.rowsAffected > 0) {
            await pool.request()
            .query(`
              SELECT
                ativo
              FROM cliente_cadastro
              WHERE
                id='${res2.recordset[0].idCliente}'
                AND ativo = 1
            `)
            .then((res3) => {
              if (res3.rowsAffected > 0) {
                array.push(arrayTemp[i])
              }
            })
            .catch(err => console.log(err))
          } else {
            await pool.request().query(`
              SELECT ativo FROM cliente_cadastro WHERE id='${arrayTemp[i].idCliente}' and ativo=1
            `)
            .then( async (res2) => {
              if (res2.rowsAffected > 0) {
                array.push(arrayTemp[i])
              }
            })
          }
  
        })
      }
    })
    res.send(array)
  } else {
    
    let clientsTemp = (req.query.id).split(',')
    let clients = []

    for (let y = 0; y < clientsTemp.length; y++) {
      await pool.request().query(`
        SELECT ativo FROM cliente_cadastro WHERE id='${clientsTemp[y]}' AND ativo=1
      `)
      .then( async (result) => {  
        if (result.rowsAffected > 0) {
          clients.push(clientsTemp[y])
        }
      })
      .catch(err => console.log(err))
    }

    if (clients.length > 0) {
      await pool.request()
      .query(`
        SELECT DISTINCT
          x.serial AS idVeiculo,
          x.idCliente,
          x.localizacao,
          x.placa,
          x.dataGPS,
          x.latitude,
          x.longitude,
          x.velocidade,
          x.tensao,
          IIF ( x.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
          IIF ( x.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
          cv.idcategoria,
          cv.idUsuario,
          cv.idCategoria as categoria,
          cv.etiqueta 
        FROM
          movimento_mt2000_frontend X
        JOIN alerts_Mt2000 amt
          ON x.MSG_TYPE = header_message
        JOIN cadastro_veiculo cv
          ON cv.placa = x.placa
        JOIN cliente_cadastro c
          ON c.id = x.idCliente
        LEFT JOIN ponto_referencia pr
          ON pr.id = x.idPontoReferencia
        LEFT JOIN area_restrita ar
          ON ar.id = x.idarearestrita
        WHERE dataGPS =
          (
            SELECT
              MAX(dataGPS)
            FROM
              movimento_mt2000_frontend i
            WHERE x.SERIAL = i.SERIAL
            AND i.LATITUDE IS NOT NULL
            AND i.LONGITUDE IS NOT NULL
            AND i.LATITUDE != ''
            AND i.LONGITUDE != ''
          )
          AND x.LATITUDE IS NOT NULL
          AND x.LONGITUDE IS NOT NULL
          AND x.LATITUDE != ''
          AND x.LONGITUDE != ''
          AND x.idCliente IN (${clients.toString().split(',')}) 
        GROUP BY
          x.serial,
          x.idCliente,
          x.localizacao,
          x.placa,
          x.dataGPS,
          x.latitude,
          x.longitude,
          x.velocidade,
          x.tensao,
          x.ignicao,
          cv.idcategoria,
          cv.idUsuario,
          cv.etiqueta
      `)
      .then( async (res1) => {
        let arrayTemp = res1.recordset
        for(let i = 0; i < arrayTemp.length; i++) {
          array.push(arrayTemp[i])
        }
      })
      res.send(array)
    } else {
      res.send(null)
    }
  }
});

app.get('/get_pos_st390', async (req, res) => {
  let array = []
  let idDoUsuario = req.query.idUser;
  let perfilVisualizacao = await GetPermissaoVisualizacao(idDoUsuario);
  if (perfilVisualizacao == 3) {
    await pool.request()
    .query(`
      SELECT * FROM
        (SELECT DISTINCT
          ms.idVeiculo,
          ms.idCliente,
          ms.idModelo,
          ms.tecnologia,
          ms.placa,
          ms.dataGPS,
          ms.dataServidor,
          ms.latitude,
          ms.longitude,
          ms.localizacao,
          ms.velocidade,
          IIF ( ms.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
          IIF ( ms.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
          ms.tensao,
          ms.odometro,
          ms.horimetro,
          cv.idUsuario,
          cv.etiqueta,
          ms.limiteVelocidadeVia,
          ROW_NUMBER() OVER (PARTITION BY idVeiculo ORDER BY dataGPS DESC ) AS RowNumber
        FROM movimento_suntech_st390_frontend ms, cadastro_veiculo cv,assoc_veiculos_usuarios asu
        WHERE asu.id_usuario = '${idDoUsuario}'
          AND cv.serial = ms.idVeiculo AND asu.id_veiculo = cv.id
        ) AS a
      WHERE a.RowNumber = 1
    `)
    .then( async (res1) => {
    
      let arrayTemp = res1.recordset
      for(let i = 0;i < arrayTemp.length; i++) {
        await pool.request().query(`
          SELECT idCliente FROM assoc_veiculo WHERE placa='${arrayTemp[i].placa}'
        `)
        .then( async (res2) => {
  
          if (res2.rowsAffected > 0) {
            await pool.request()
            .query(`
              SELECT
                ativo
              FROM cliente_cadastro
              WHERE
                id='${res2.recordset[0].idCliente}'
                AND ativo = 1
            `)
            .then((res3) => {
              if (res3.rowsAffected > 0) {
                array.push(arrayTemp[i])
              }
            })
            .catch(err => console.log(err))
          } else {
            await pool.request().query(`
              SELECT ativo FROM cliente_cadastro WHERE id='${arrayTemp[i].idCliente}' and ativo=1
            `)
            .then( async (res2) => {
              if (res2.rowsAffected > 0) {
                array.push(arrayTemp[i])
              }
            })
          }
  
        })
      }
    })
    res.send(array)
  } else {
    let clientsTemp = (req.query.id).split(',')
    let clients = []

    for (let y = 0; y < clientsTemp.length; y++) {
      await pool.request().query(`
        SELECT ativo FROM cliente_cadastro WHERE id='${clientsTemp[y]}' AND ativo=1
      `)
      .then( async (result) => {  
        if (result.rowsAffected > 0) {
          clients.push(clientsTemp[y])
        }
      })
      .catch(err => console.log(err))
    }

    if (clients.length > 0) {
      await pool.request()
      .query(`
        SELECT * FROM
          (SELECT DISTINCT
            ms.idVeiculo,
            ms.idCliente,
            ms.idModelo,
            ms.tecnologia,
            ms.placa,
            ms.dataGPS,
            ms.dataServidor,
            ms.latitude,
            ms.longitude,
            ms.localizacao,
            ms.velocidade,
            IIF ( ms.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
            IIF ( ms.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
            ms.tensao,
            ms.odometro,
            ms.horimetro,
            cv.idUsuario,
            cv.etiqueta,
            ms.limiteVelocidadeVia,
            ROW_NUMBER() OVER (PARTITION BY idVeiculo ORDER BY dataGPS DESC ) AS RowNumber
          FROM movimento_suntech_st390_frontend ms, cadastro_veiculo cv
          WHERE ms.idCliente in (${clients.toString().split(',')})
            AND cv.serial = ms.idVeiculo 
          ) AS a
        WHERE a.RowNumber = 1
      `)
      .then( async (res1) => {
        let arrayTemp = res1.recordset
        for(let i = 0; i < arrayTemp.length; i++) {
          array.push(arrayTemp[i])
        }
      })
      res.send(array)
    } else {
      res.send(null)
    }
  }
});

app.get('/get_pos_suntech410', async (req, res) => {
  let array = []
  let idDoUsuario = req.query.idUser;
  let perfilVisualizacao = await GetPermissaoVisualizacao(idDoUsuario);
  if (perfilVisualizacao == 3) {
    await pool.request()
    .query(`
      SELECT * FROM
        (SELECT DISTINCT
          ms.idVeiculo,
          ms.idCliente,
          ms.idModelo,
          ms.tecnologia,
          ms.placa,
          ms.dataGPS,
          ms.dataServidor,
          ms.latitude, 
          ms.longitude,
          ms.localizacao,
          ms.velocidade,
          IIF ( ms.online = 1, 'Ligado', 'Desligado') AS ignicao,
          IIF ( ms.online = 1, '#00EB00', '#FF0322' ) AS statusVeic,
          ms.battery AS tensao,
          ms.temperatura,
          cv.idUsuario,
          cv.etiqueta,
          ROW_NUMBER() OVER (PARTITION BY idVeiculo ORDER BY dataGPS DESC ) AS RowNumber
        FROM movimento_suntech_frontend_410 ms, cadastro_veiculo cv,assoc_veiculos_usuarios asu
        WHERE asu.id_usuario = '${idDoUsuario}'
          AND cv.serial = ms.idVeiculo
          AND asu.id_veiculo = cv.id
        ) AS a
      WHERE a.RowNumber = 1
    `)
    .then( async (res1) => {
    
      let arrayTemp = res1.recordset
      for(let i = 0;i < arrayTemp.length; i++) {
        await pool.request().query(`
          SELECT ativo FROM cliente_cadastro WHERE id='${arrayTemp[i].idCliente}' and ativo=1
        `)
        .then( async (res2) => {
          if (res2.rowsAffected > 0) {
            array.push(arrayTemp[i])
          }
        })
      }
    })
    res.send(array)
  } else {
    let clientsTemp = (req.query.id).split(',')
    let clients = []

    for (let y = 0; y < clientsTemp.length; y++) {
      await pool.request().query(`
        SELECT ativo FROM cliente_cadastro WHERE id='${clientsTemp[y]}' AND ativo=1
      `)
      .then( async (result) => {  
        if (result.rowsAffected > 0) {
          clients.push(clientsTemp[y])
        }
      })
      .catch(err => console.log(err))
    }

    if (clients.length > 0) {
      await pool.request()
      .query(`
        SELECT * FROM
          (SELECT DISTINCT
            ms.idVeiculo,
            ms.idCliente,
            ms.idModelo,
            ms.tecnologia,
            ms.placa,
            ms.dataGPS,
            ms.dataServidor,
            ms.latitude,
            ms.longitude,
            ms.localizacao,
            ms.velocidade,
            IIF ( ms.online = 1, 'Ligado', 'Desligado') AS ignicao,
            IIF ( ms.online = 1, '#00EB00', '#FF0322' ) AS statusVeic,
            ms.battery AS tensao,
            cv.idUsuario,
            cv.etiqueta,
            ROW_NUMBER() OVER (PARTITION BY idVeiculo ORDER BY dataGPS DESC ) AS RowNumber
          FROM movimento_suntech_frontend_410 ms, cadastro_veiculo cv
          WHERE ms.idCliente in (${clients.toString().split(',')})
            AND cv.serial = ms.idVeiculo 
          ) AS a
        WHERE a.RowNumber = 1
      `)
      .then( async (res1) => {
        let arrayTemp = res1.recordset
        for(let i = 0; i < arrayTemp.length; i++) {
          array.push(arrayTemp[i])
        }
      })
      res.send(array)
    } else {
      res.send(null)
    }
  }
});

app.get('/get_pos_j16', async (req, res) => {
  let array = []
  let idDoUsuario = req.query.idUser;
  let perfilVisualizacao = await GetPermissaoVisualizacao(idDoUsuario);
  if (perfilVisualizacao == 3) {
    await pool.request()
    .query(`
      SELECT * FROM
        (SELECT DISTINCT
          m.serial AS idVeiculo,
          m.id_cliente AS idCliente,
          m.placa,
          m.dataGPS,
          m.latitude, 
          m.longitude,
          m.endereco AS localizacao,
          m.velocidade,
          IIF ( m.ignicao = 1, 'Ligado', 'Desligado') AS ignicao,
          IIF ( m.ignicao = 1, '#00EB00', '#FF0322' ) AS statusVeic,
          m.tensao_bateria_interna AS tensao,
          cv.idUsuario,
          cv.etiqueta,
          ROW_NUMBER() OVER (PARTITION BY m.serial ORDER BY m.dataGPS DESC ) AS RowNumber
        FROM movimento_j16_frontend m, cadastro_veiculo cv,assoc_veiculos_usuarios asu
        WHERE asu.id_usuario = '${idDoUsuario}'
          AND cv.serial = m.serial
          AND asu.id_veiculo = cv.id
        ) AS a
      WHERE a.RowNumber = 1 
    `)
    .then( async (res1) => {
    
      let arrayTemp = res1.recordset
      for(let i = 0;i < arrayTemp.length; i++) {
        await pool.request().query(`
          SELECT idCliente FROM assoc_veiculo WHERE placa='${arrayTemp[i].placa}'
        `)
        .then( async (res2) => {
  
          if (res2.rowsAffected > 0) {
            await pool.request()
            .query(`
              SELECT
                ativo
              FROM cliente_cadastro
              WHERE
                id='${res2.recordset[0].idCliente}'
                AND ativo = 1
            `)
            .then((res3) => {
              if (res3.rowsAffected > 0) {
                array.push(arrayTemp[i])
              }
            })
            .catch(err => console.log(err))
          } else {
            await pool.request().query(`
              SELECT ativo FROM cliente_cadastro WHERE id='${arrayTemp[i].idCliente}' and ativo=1
            `)
            .then( async (res2) => {
              if (res2.rowsAffected > 0) {
                array.push(arrayTemp[i])
              }
            })
          }
  
        })
      }
    })
    res.send(array)
  } else {
    let clientsTemp = (req.query.id).split(',')
    let clients = []

    for (let y = 0; y < clientsTemp.length; y++) {
      await pool.request().query(`
        SELECT ativo FROM cliente_cadastro WHERE id='${clientsTemp[y]}' AND ativo=1
      `)
      .then( async (result) => {  
        if (result.rowsAffected > 0) {
          clients.push(clientsTemp[y])
        }
      })
      .catch(err => console.log(err))
    }
    if (clients.length > 0) {
      await pool.request()
      .query(`
        SELECT * FROM
          (SELECT DISTINCT
            m.serial AS idVeiculo,
            m.id_cliente AS idCliente,
            m.placa,
            m.dataGPS,
            m.latitude, 
            m.longitude,
            m.endereco AS localizacao,
            m.velocidade,
            IIF ( m.ignicao = 1, 'Ligado', 'Desligado') AS ignicao,
            IIF ( m.ignicao = 1, '#00EB00', '#FF0322' ) AS statusVeic,
            m.tensao_bateria_interna AS tensao,
            cv.idUsuario,
            cv.etiqueta,
            ROW_NUMBER() OVER (PARTITION BY m.serial ORDER BY m.dataGPS DESC ) AS RowNumber
          FROM movimento_j16_frontend m, cadastro_veiculo cv
          WHERE m.id_cliente in (${clients.toString().split(',')})
            AND cv.serial = m.serial 
          ) AS a
        WHERE a.RowNumber = 1
      `)
      .then( async (res1) => {
        let arrayTemp = res1.recordset
        for(let i = 0; i < arrayTemp.length; i++) {
          array.push(arrayTemp[i])
        }
      })
      res.send(array)
    } else {
      res.send(null)
    }
  }
});

// --------------------------------------------------------- POSITION DETAIL
app.get('/get_pos_gv57_detail', (req, res) => {
  execSQLQuery(`
  SELECT TOP 1
    a.uniqueid as idVeiculo,
    a.idCliente,
    a.latitude,
    a.longitude,
    a.placa,
    FORMAT(DATEADD (HOUR, CAST(0 AS INTEGER), a.utctime), 'yyyy-MM-dd HH:mm:ss' ) AS dataGPS,
    a.localizacao,
    a.speed AS velocidade,
    IIF ( a.ignicao = 0, 'Ligado', 'Desligado') as ignicao,
    IIF ( a.ignicao = 0, '#00EB00', '#FF0322' ) as statusVeic,
    a.mileage as horimetro
  FROM movimento_gv57_frontend a
	  LEFT JOIN eventos_globais b ON b.id = a.event
  WHERE a.uniqueid = '${req.query.idV}'
  ORDER BY dataGPS DESC`, res);
});

app.get('/get_pos_gv_detail', (req, res) => {
  execSQLQuery(`
  SELECT TOP 1
    a.uniqueid as idVeiculo,
    a.idCliente,
    a.latitude,
    a.longitude,
    a.placa,
    FORMAT(DATEADD (HOUR, CAST(- 3 AS INTEGER), a.utctime), 'yyyy-MM-dd HH:mm:ss' ) AS dataGPS,
    a.localizacao,
    a.speed AS velocidade,
    IIF ( a.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
    IIF ( a.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
    a.mileage as horimetro,
    m.nome AS motorista
  FROM movimento_gv75_frontend a
	  LEFT JOIN eventos_globais b ON b.id = a.event
    LEFT JOIN motorista m ON m.rfid = a.gsRfid
  WHERE a.uniqueid = '${req.query.idV}'
  ORDER BY dataGPS DESC`, res);
});

app.get('/get_pos_trac_detail', (req, res) => {
  execSQLQuery(`
  SELECT TOP 1
    m.serial as idVeiculo, 
    IIF ( m.alertaIgnicao = 1, 'Ligado', 'Desligado') as ignicao,
    IIF ( m.alertaIgnicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
    m.tensao, 
    m.placa, 
    cast(m.velocidade as numeric(10, 0)) as velocidade,
    m.longitude, 
    m.latitude, 
    m.localizacao, 
    m.dataServidor,
    m.dataGPS,
    m.hodometro, 
    m.idCliente,
    m.horimetro,
    e.descricao as tecnologia,
    mm.nome AS motorista
  FROM movimento_grcad_frontend m, evento_grcad e
    LEFT JOIN motorista mm ON m.rfid = m.gsRfid
  WHERE m.serial = '${req.query.idV}'
    AND m.evento = e.id
  ORDER BY dataGPS DESC`, res);
});

app.get('/get_pos_suntech_detail', (req, res) => {
  pool.request().query(`
    SELECT TOP 1 
      mm.idVeiculo,
      mm.idCliente,
      mm.idModelo,
      mm.placa,
      mm.dataGPS,
      mm.dataServidor,
      mm.latitude,
      mm.longitude,
      mm.localizacao,
      CAST(CAST(mm.velocidade AS FLOAT) AS INT) as velocidade,
      IIF ( mm.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( mm.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      mm.tensao,
      mm.odometro,
      mm.temperaturaS1,
      c.eventDesc as tecnologia,
      m.nome AS motorista
    FROM movimento_suntech mm
      INNER JOIN(
        SELECT id, eventDesc FROM eventos_globais
      ) c ON mm.evento = c.id
      LEFT JOIN motorista m ON m.rfid = mm.gsRfid
    WHERE mm.idVeiculo = '${req.query.idV}'
    ORDER BY m.id DESC
  `)
  .then(result => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_mt2000_detail', (req, res) => {
  execSQLQuery(`
    SELECT
      mt.serial as idVeiculo,
      mt.idCliente,
      mt.latitude,
      mt.longitude,
      mt.placa,
      mt.dataGPS,
      mt.dataServidor,
      mt.localizacao,
      mt.velocidade,
      IIF ( mt.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( mt.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      mt.tensao,
      mt.ODEMETER_TOTAL as odometro,
      c.eventDesc as tecnologia
    FROM
      movimento_mt2000_frontend as mt
    INNER JOIN(
      SELECT id, eventDesc FROM eventos_globais
    ) c ON mt.evento = c.id,
      (select PLACA, max(datagps) AS DATA 
          from movimento_mt2000_frontend 
        WHERE datalength(longitude) > 0   
          AND datalength(latitude) > 0
        GROUP BY PLACA) A
    WHERE
      datalength(mt.longitude) > 0
    AND datalength(mt.latitude) > 0
    AND mt.DATAGPS = A.DATA
    AND mt.SERIAL = '${req.query.idV}'
  ORDER BY datagps DESC`, res);
});

app.get('/get_pos_teltonika_detail', (req, res) => {
  execSQLQuery(`
  SELECT TOP 1
    a.serial as idVeiculo,
    a.idCliente,
    a.latitude,
    a.longitude,
    a.temperaturaS2,
    a.umidadeS2,
    a.placa,
    a.dataServidor,
    a.endereco,
    a.velocidade2,
    IIF ( a.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
    IIF ( a.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
    a.TotalOdometro,
	  a.evento,
    CASE WHEN a.evento = 0 THEN 'Normal'
      ELSE b.eventDesc
      END as tecnologia
  FROM movimento_teltonika_frontend as a
	  LEFT JOIN eventos_globais b ON b.id = a.evento
  WHERE a.SERIAL = '${req.query.idV}'
    AND a.temperaturaS2 <> 'NaN'
    AND dataGPS <> 'Invalid Date'
  ORDER BY dataServidor DESC`, res);
});

app.get('/get_pos_vl01_detail', (req, res) => {
  execSQLQuery(`
  SELECT TOP 1
    a.serial as idVeiculo,
    a.id_cliente,
    CAST (a.latitude AS FLOAT) * -1 AS latitude,
    CAST (a.longitude AS FLOAT) * -1 AS longitude,
    a.placa,
    a.dataGPS,
    a.endereco,
    a.velocidade,
    IIF ( a.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
    IIF ( a.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
    a.odometro,
    a.tensao_bateria_interna,
    a.evento,
    CASE WHEN a.evento = 0 THEN 'Normal'
      ELSE b.eventDesc
      END as tecnologia
  FROM movimento_vl01_frontend as a
    LEFT JOIN eventos_globais b ON b.id = a.evento
  WHERE a.SERIAL = '${req.query.idV}'
  ORDER BY dataGPS DESC`, res);
});

app.get('/get_pos_vl02_detail', (req, res) => {
  execSQLQuery(`
  SELECT TOP 1
    a.serial as idVeiculo,
    a.id_cliente,
    CAST (a.latitude AS FLOAT) * -1 AS latitude,
    CAST (a.longitude AS FLOAT) * -1 AS longitude,
    a.placa,
    a.dataGPS,
    a.endereco,
    a.velocidade,
    IIF ( a.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
    IIF ( a.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
    a.temperaturaS1 as temperatura,
    a.odometro,
    a.tensao_bateria_interna,
    a.evento,
    CASE WHEN a.evento = 0 THEN 'Normal'
      ELSE b.eventDesc
      END as tecnologia,
    m.nome AS motorista
  FROM movimento_vl02 as a
    LEFT JOIN eventos_globais b ON b.id = a.evento
    LEFT JOIN motorista m ON m.rfid = a.gsRfid
  WHERE a.SERIAL = '${req.query.idV}'
  ORDER BY dataGPS DESC`, res);
});

app.get('/get_pos_vl03_detail', (req, res) => {
  execSQLQuery(`
  SELECT TOP 1
    a.serial as idVeiculo,
    a.id_cliente,
    CAST (a.latitude AS FLOAT) * -1 AS latitude,
    CAST (a.longitude AS FLOAT) * -1 AS longitude,
    a.placa,
    a.dataGPS,
    a.endereco,
    a.velocidade,
    IIF ( a.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
    IIF ( a.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
    a.odometro,
    a.tensao_bateria_interna,
    a.evento,
    CASE WHEN a.evento = 0 THEN 'Normal'
      ELSE b.eventDesc
      END as tecnologia
  FROM movimento_vl03_frontend as a
    LEFT JOIN eventos_globais b ON b.id = a.evento
  WHERE a.SERIAL = '${req.query.idV}'
  ORDER BY dataGPS DESC`, res);
});

app.get('/get_pos_st390_detail', (req, res) => {
  execSQLQuery(`
  SELECT TOP 1 
    idVeiculo,
    idCliente,
    idModelo,
    placa,
    dataGPS,
    dataServidor,
    latitude,
    longitude,
    localizacao,
    velocidade,
    IIF ( ignicao = 1, 'Ligado', 'Desligado') as ignicao,
    IIF ( ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
    tensao,
    odometro,
    eventDesc as tecnologia
  FROM movimento_suntech_st390 m
  INNER JOIN(
    SELECT id, eventDesc FROM eventos_globais
  ) c ON m.evento = c.id
  WHERE idVeiculo = '${req.query.idV}'
  ORDER BY m.id DESC`, res);
});

app.get('/get_pos_suntech410_detail', (req, res) => {
  execSQLQuery(`
    SELECT TOP 1 
      idVeiculo,
      idCliente,
      idModelo,
      placa,
      dataGPS,
      latitude,
      longitude,
      localizacao,
      velocidade,
      IIF ( online = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( online = 1, '#00EB00', '#FF0322' ) as statusVeic,
      battery AS tensao,
      temperatura
    FROM movimento_suntech410
    WHERE idVeiculo = '${req.query.idV}'
    ORDER BY id DESC
  `, res);
});

app.get('/get_pos_j16_detail', (req, res) => {
  execSQLQuery(`
    SELECT TOP 1 
      serial AS idVeiculo,
      id_cliente AS idCliente,
      placa,
      dataGPS,
      latitude, 
      longitude,
      endereco AS localizacao,
      velocidade,
      IIF ( ignicao = 1, 'Ligado', 'Desligado') AS ignicao,
      IIF ( ignicao = 1, '#00EB00', '#FF0322' ) AS statusVeic,
      tensao_bateria_interna AS tensao
    FROM movimento_j16_frontend
    WHERE serial = '${req.query.idV}'
    ORDER BY id DESC
  `, res);
});

//  --------------------------------------------------------- HISTORY
app.get('/get_pos_hist_trac', (req, res) => {
  if (req.query.id == 100 || req.query.id == 1) {
    pool.request().query(`
      SELECT
        x.serial as idVeiculo, 
        IIF ( x.alertaIgnicao = 1, 'Ligado', 'Desligado') as ignicao,
        IIF ( x.alertaIgnicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
        x.tensao, 
        x.placa, 
        g.descricao as evento, 
        cast(x.velocidade as numeric(10, 0)) as velocidade,
        x.longitude, 
        x.latitude, 
        x.localizacao, 
        x.dataServidor as dataServidor,
        x.dataGPS as dataGPS,
        x.hodometro, 
        cv.etiqueta,
        x.idCliente,
        e.descricao,
        m.nome AS motorista
      FROM movimento_grcad x 
        JOIN evento_grcad g ON x.evento = g.id 
        JOIN cadastro_veiculo cv ON cv.placa = x.placa 
        LEFT JOIN motorista m ON m.rfid = x.gsRfid
      WHERE x.idAdm = '100'
        AND x.placa is not null 
        AND x.placa = '${req.query.place}'
        AND x.dataGPS BETWEEN '${req.query.dataIni}'
        AND  '${req.query.dataFim}'
      ORDER BY x.dataGPS ${req.query.order || 'DESC'}`)
    .then((result) => {
      res.send(result.recordset)
    })
  } else {
    pool.request().query(`
      SELECT
        x.serial as idVeiculo, 
        IIF ( x.alertaIgnicao = 1, 'Ligado', 'Desligado') as ignicao,
        IIF ( x.alertaIgnicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
        x.tensao, 
        x.placa, 
        g.descricao as evento, 
        cast(x.velocidade as numeric(10, 0)) as velocidade,
        x.longitude, 
        x.latitude, 
        x.localizacao, 
        x.dataServidor as dataServidor,
        x.dataGPS as dataGPS,
        x.hodometro, 
        cv.etiqueta,
        x.idCliente,
        m.nome AS motorista
      FROM movimento_grcad x 
        JOIN evento_grcad g ON x.evento = g.id 
        JOIN cadastro_veiculo cv ON cv.placa = x.placa 
        LEFT JOIN motorista m ON m.rfid = x.gsRfid
      WHERE  x.placa is not null 
        AND x.placa = '${req.query.place}'
        AND x.dataGPS BETWEEN '${req.query.dataIni}' AND  '${req.query.dataFim}'
      ORDER BY x.dataGPS ${req.query.order || 'DESC'}
    `)
    .then((result) => {
      res.send(result.recordset)
    })
  }
});

app.get('/get_pos_hist_gv57', (req, res) => {
  pool.request().query(`
    SELECT
      countnumber,
      cv.etiqueta,
      IIF ( x.ignicao = 0, 'Ligado', 'Desligado') as ignicao,
      IIF ( x.ignicao = 0, '#00EB00', '#FF0322' ) as statusVeic,
      x.placa,
      speed AS velocidade,
      longitude,
      latitude,
      localizacao,
      uniqueid AS idVeiculo,
      FORMAT(
        msasDataServidor, 'yyyy-MM-dd HH:mm:ss'
      ) AS dataGPS,
      ROUND(
        CAST(externalpower AS FLOAT) / 1000, 2
      ) AS tensao,
      x.mileage AS odometro,
      m.nome AS motorista
    FROM movimento_gv57 X
    LEFT JOIN cadastro_veiculo cv
      ON cv.placa = x.placa
    LEFT JOIN motorista m ON m.rfid = x.gsRfid
    WHERE x.placa = '${req.query.place}'
      AND x.msasDataServidor BETWEEN '${req.query.dataIni}' AND '${req.query.dataFim}'
    ORDER BY x.msasDataServidor ASC
  `)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_gv75', (req, res) => {
  pool.request().query(`
    SELECT
      countnumber,
      cv.etiqueta,
      IIF ( x.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( x.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      x.placa,
      speed AS velocidade,
      CONCAT(
        COALESCE(g.alertDesc, '-----'), ' / ', COALESCE(h.alertDesc, '-----')
      ) AS tecnologia,
      longitude,
      latitude,
      localizacao,
      uniqueid AS idVeiculo,
      FORMAT(
        msasDataServidor, 'yyyy-MM-dd HH:mm:ss'
      ) AS dataGPS,
      ROUND(
        CAST(externalpower AS FLOAT) / 1000, 2
      ) AS tensao,
      x.mileage AS odometro
    FROM movimento_gv75 X
    LEFT JOIN alerts_gv75 g
      ON x.event = g.alertValue
        AND g.alertType NOT LIKE 'REPORTIDTYPE'
        AND x.header = g.alertHeader
    LEFT JOIN alerts_gv75 h
      ON x.eventTypeId = h.alertValue
        AND x.header = h.alertHeader
        AND h.alertType LIKE 'REPORTIDTYPE'
    LEFT JOIN cadastro_veiculo cv
      ON cv.placa = x.placa
    WHERE x.placa = '${req.query.place}'
      AND x.msasDataServidor BETWEEN '${req.query.dataIni}' AND '${req.query.dataFim}'
    ORDER BY x.msasDataServidor ASC`)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_suntech', (req, res) => {
  pool.request().query(`
    SELECT
      mm.idVeiculo,
      mm.idCliente,
      mm.idModelo,
      mm.tecnologia,
      mm.placa,
      mm.dataGPS,
      mm.dataServidor,
      mm.latitude,
      mm.longitude,
      mm.localizacao,
      CAST(CAST(mm.velocidade AS FLOAT) AS INT) as velocidade,
      IIF ( mm.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( mm.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      mm.tensao,
      mm.odometro,
      mm.temperaturaS1,
      CASE 
        WHEN (mm.tipo = 'STT') THEN (select eventDesc from eventos_suntech_stt a where evento = a.codEvento )
        WHEN (mm.tipo = 'ALT') THEN (select eventDesc from eventos_suntech_alt a where evento = a.codEvento )
        WHEN (mm.tipo = 'EMG') THEN (select eventoDesc from eventos_suntech_emg a where evento = a.codEvento )
        WHEN (mm.tipo = 'EVT') THEN (select eventDesc from eventos_suntech_evt a where evento = a.codEvento )
      ELSE '' END evento,
      m.nome AS motorista
    FROM movimento_suntech mm
      LEFT JOIN motorista m ON m.rfid = mm.gsRfid
    WHERE mm.placa = '${req.query.place}'
      AND mm.dataGPS BETWEEN '${req.query.dataIni}'
      AND '${req.query.dataFim}'
    ORDER BY mm.dataGPS ASC`)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_mt2000', (req, res) => {
  pool.request().query(`
    SELECT
      serial as idVeiculo,
      idCliente,
      localizacao,
      placa,
      dataGPS,
      latitude,
      longitude,
      IIF ( ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      TENSAO,
      velocidade,
      ODEMETER_TOTAL as odometro
    FROM
      movimento_mt2000
    WHERE
      datalength(longitude) > 0
      AND datalength(latitude) > 0
      AND dataGPS BETWEEN '${req.query.dataIni}' AND '${req.query.dataFim}'
      AND placa = '${req.query.place}'
    GROUP BY
      serial,
      idCliente,
      localizacao,
      placa,
      dataGPS,
      IGNICAO,
      TENSAO,
      latitude, 
      longitude,
      velocidade,
      ODEMETER_TOTAL
    ORDER BY datagps ASC`)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_teltonika', (req, res) => {
  pool.request().query(`
    SELECT  
      x.serial,
      x.idCliente,
      x.placa,
      x.latitude,
      x.longitude,
      x.umidadeS2,
      x.temperaturaS2,
      x.endereco,
      x.velocidade2,
      x.dataServidor,
      IIF ( x.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( x.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      x.TotalOdometro,
      x.bateria_externa,
      e.descEvento AS evento
    FROM  movimento_teltonika x
      LEFT JOIN evento_teltonika e ON e.idEvento = x.evento
    WHERE placa = '${req.query.place}'
      AND dataServidor BETWEEN '${req.query.dataIni}' AND '${req.query.dataFim}'
      AND temperaturaS2 <> 'NaN'
      AND dataGPS <> 'Invalid Date'
    ORDER BY dataServidor ASC`)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_vl01', (req, res) => {
  pool.request().query(`
    SELECT  
      x.serial,
      x.id_cliente,
      x.placa,
      CAST (x.latitude AS FLOAT) * 1 AS latitude,
      CAST (x.longitude AS FLOAT) * 1 AS longitude,
      x.endereco,
      x.velocidade,
      IIF ( x.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( x.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      x.odometro,
      x.dataGPS,
      e.id_evento AS evento
    FROM movimento_vl01 x
     LEFT JOIN evento_vl01 e ON e.id_Evento = x.evento
    WHERE placa = '${req.query.place}'
      AND dataGPS BETWEEN '${req.query.dataIni}' AND '${req.query.dataFim}'
    ORDER BY dataGPS ASC
  `)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_vl02', (req, res) => {
  pool.request().query(`
    SELECT  
      x.serial,
      x.id_cliente,
      x.placa,
      CAST (x.latitude AS FLOAT) * 1 AS latitude,
      CAST (x.longitude AS FLOAT) * 1 AS longitude,
      x.endereco,
      x.velocidade,
      IIF ( x.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( x.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      x.odometro,
      x.dataGPS,
      x.temperaturaS1 as temperatura,
      e.id_evento AS evento,
      m.nome AS motorista
    FROM movimento_vl02 x
      INNER JOIN evento_vl02 e ON e.id_Evento = x.evento
      LEFT JOIN motorista m ON m.rfid = x.rfid
    WHERE x.placa = '${req.query.place}'
      AND x.dataGPS BETWEEN '${req.query.dataIni}' AND '${req.query.dataFim}'
    ORDER BY dataGPS ASC
  `)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_vl03', (req, res) => {
  pool.request().query(`
    SELECT  
      x.serial,
      x.id_cliente,
      x.placa,
      CAST (x.latitude AS FLOAT) * -1 AS latitude,
      CAST (x.longitude AS FLOAT) * -1 AS longitude,
      x.endereco,
      x.velocidade,
      IIF ( x.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF ( x.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      x.odometro,
      x.dataGPS,
      e.id_evento AS evento
    FROM movimento_vl03 x
      INNER JOIN evento_vl03 e ON e.id_Evento = x.evento
    WHERE placa = '${req.query.place}'
      AND dataGPS BETWEEN '${req.query.dataIni}' AND '${req.query.dataFim}'
    ORDER BY dataGPS ASC
  `)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_st390', (req, res) => {
  pool.request().query(`
    SELECT
      x.idVeiculo,
      x.idCliente,
      x.idModelo,
      x.tecnologia,
      x.placa,
      x.dataGPS,
      x.dataServidor,
      x.latitude,
      x.longitude,
      x.localizacao,
      x.velocidade,
      IIF (x.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      IIF (x.ignicao = 1, '#00EB00', '#FF0322' ) as statusVeic,
      x.tensao,
      x.odometro,
      e.eventDesc AS evento
    FROM movimento_suntech_st390 x
     INNER JOIN eventos_suntech_st390 e ON e.codEvento = x.evento
    WHERE placa = '${req.query.place}'
      AND dataGPS BETWEEN '${req.query.dataIni}' AND  '${req.query.dataFim}'
    ORDER BY dataGPS ASC`)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_suntech410', (req, res) => {
  pool.request().query(`
    SELECT
      idVeiculo,
      idCliente,
      idModelo,
      tecnologia,
      placa,
      dataGPS,
      latitude,
      longitude,
      localizacao,
      velocidade,
      IIF (online = 1, 'Ligado', 'Desligado') as ignicao,
      IIF (online = 1, '#00EB00', '#FF0322' ) as statusVeic,
      battery AS tensao,
      temperatura
    FROM movimento_suntech410
    WHERE placa = '${req.query.place}'
      AND dataGPS BETWEEN '${req.query.dataIni}' AND  '${req.query.dataFim}'
    ORDER BY dataGPS ASC`)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_j16', (req, res) => {
  pool.request().query(`
    SELECT
      serial AS idVeiculo,
      id_cliente AS idCliente,
      placa,
      dataGPS,
      latitude, 
      longitude,
      endereco AS localizacao,
      velocidade,
      IIF ( ignicao = 1, 'Ligado', 'Desligado') AS ignicao,
      IIF ( ignicao = 1, '#00EB00', '#FF0322' ) AS statusVeic,
      tensao_bateria_interna AS tensao
    FROM movimento_j16
    WHERE placa = '${req.query.place}'
      AND dataGPS BETWEEN '${req.query.dataIni}' AND '${req.query.dataFim}'
    ORDER BY dataGPS ASC`)
  .then((result) => {
    res.send(result.recordset)
  })
});

// --------------------------------------------------------- HISTORY DETAILS
app.get('/get_pos_hist_trac_detail', (req, res) => {
  if (req.query.id == 100 || req.query.id == 1) {
    pool.request().query(`
      SELECT TOP 1
        IIF (alertaIgnicao = 1, 'Ligado', 'Desligado') as ignicao,
        x.tensao, 
        x.placa, 
        cast(x.velocidade as numeric(10, 0)) as velocidade,
        x.localizacao, 
        x.dataGPS as dataGPS,
        x.hodometro,
        m.nome AS motorista
      FROM movimento_grcad x 
        JOIN evento_grcad g ON x.evento = g.id 
        JOIN cadastro_veiculo cv ON cv.placa = x.placa 
        LEFT JOIN motorista m ON m.rfid = x.gsRfid
      WHERE x.idAdm = '100'
        AND x.placa is not null 
        AND x.serial = '${req.query.idV}'
        AND x.dataGPS = '${req.query.dataGps}'
    `)
    .then((result) => {
      res.send(result.recordset)
    })
  } else {
    pool.request().query(`
      SELECT TOP 1
        x.serial as idVeiculo, 
        IIF (alertaIgnicao = 1, 'Ligado', 'Desligado') as ignicao,
        x.tensao, 
        x.placa, 
        g.descricao as evento, 
        cast(x.velocidade as numeric(10, 0)) as velocidade,
        x.localizacao, 
        x.dataServidor as dataServidor,
        x.dataGPS as dataGPS,
        x.hodometro, 
        cv.etiqueta,
        x.idCliente,
        m.nome AS motorista
      FROM movimento_grcad x 
        JOIN evento_grcad g ON x.evento = g.id 
        JOIN cadastro_veiculo cv ON cv.placa = x.placa 
        LEFT JOIN motorista m ON m.rfid = x.gsRfid
        WHERE x.placa is not null 
          AND x.serial = '${req.query.idV}'
          AND x.dataGPS = '${req.query.dataGps}'
    `)
    .then((result) => {
      res.send(result.recordset)
    })
  }
});

app.get('/get_pos_hist_gv57_detail', (req, res) => {
  pool.request().query(`
    SELECT TOP 1 
      IIF (m.ignicao = 0, 'Ligado', 'Desligado') as ignicao,
      m.placa,
      FORMAT(
        m.msasDataServidor,
        'yyyy-MM-dd HH:mm:ss'
      ) AS dataGPS,
      m.localizacao,
      m.speed AS velocidade,
      m.externalpower AS tensao,
      m.mileage as odometro,
      mt.nome AS motorista
    FROM movimento_gv57 m
      INNER JOIN(
        SELECT id, eventDesc FROM eventos_globais
      ) c ON m.event = c.id
      LEFT JOIN motorista mt ON mt.rfid = x.gsRfid
    WHERE m.uniqueid = '${req.query.idV}'
      AND m.msasDataServidor = '${req.query.dataGps}'
    ORDER BY m.id DESC
  `)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_gv_detail', (req, res) => {
  pool.request().query(`
    SELECT TOP 1 
      IIF (ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      placa,
      FORMAT(
        msasDataServidor,
        'yyyy-MM-dd HH:mm:ss'
      ) AS dataGPS,
      localizacao,
      speed AS velocidade,
      externalpower AS tensao,
      mileage as odometro
    FROM movimento_gv75 m
    INNER JOIN(
      SELECT id, eventDesc FROM eventos_globais
    ) c ON m.event = c.id
    WHERE uniqueid = '${req.query.idV}'
      AND msasDataServidor = '${req.query.dataGps}'
    ORDER BY m.id DESC
  `)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_suntech_detail', (req, res) => {
  pool.request().query(`
    SELECT TOP 1
      x.placa,
      x.dataGPS,
      x.localizacao,
      CAST(CAST(x.velocidade AS FLOAT) AS INT) as velocidade,
      IIF (x.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      x.tensao,
      x.odometro,
      m.nome AS motorista
    FROM movimento_suntech x
      LEFT JOIN motorista m ON m.rfid = x.gsRfid
    WHERE x.idVeiculo = '${req.query.idV}'
      AND x.dataGPS = '${req.query.dataGps}'
  `)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_mt2000_detail', (req, res) => {
    pool.request().query(`
      SELECT
        IIF (ignicao = 1, 'Ligado', 'Desligado') as ignicao,
        placa,
        direcao,
        velocidade,
        ODEMETER_TOTAL AS odometro,
        tensao,
        dataGPS,
        localizacao
      FROM
        movimento_mt2000
      WHERE
        datalength(longitude) > 0
      AND datalength(latitude) > 0
      AND dataGPS = '${req.query.dataGPS}'
      AND serial = ${req.query.idV}
    `)
    .then((result) => {
      res.send(result.recordset)
    })
});

app.get('/get_pos_hist_teltonika_detail', (req, res) => {
  pool.request().query(`
    SELECT TOP 1
      placa,
      umidadeS2,
      temperaturaS2,
      dataServidor,
      endereco,
      velocidade2,
      bateria_externa,
      IIF (ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      TotalOdometro
    FROM
      movimento_teltonika
    WHERE serial = ${req.query.idV}
      AND dataServidor = '${req.query.dataGPS}'
  `)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_vl01_detail', (req, res) => {
  pool.request().query(`
    SELECT TOP 1
      serial,
      id_cliente,
      placa,
      endereco,
      dataGPS,
      velocidade,
      tensao_bateria_externa,
      IIF (ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      odometro
    FROM
      movimento_vl01
    WHERE serial = ${req.query.idV}
      AND dataGPS = '${req.query.dataGPS}'
  `)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_vl02_detail', (req, res) => {
  pool.request().query(`
    SELECT TOP 1
      x.serial,
      x.id_cliente,
      x.placa,
      x.endereco,
      x.dataGPS,
      x.velocidade,
      x.tensao_bateria_externa,
      IIF (x.ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      x.odometro,
      x.temperaturaS1 as temperatura,
      m.nome AS motorista
    FROM movimento_vl02 x
      LEFT JOIN motorista m ON m.rfid = x.rfid
    WHERE x.serial = ${req.query.idV}
      AND x.dataGPS = '${req.query.dataGPS}'
  `)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_vl03_detail', (req, res) => {
  pool.request().query(`
    SELECT TOP 1
      serial,
      id_cliente,
      placa,
      endereco,
      dataGPS,
      velocidade,
      tensao_bateria_externa,
      IIF (ignicao = 1, 'Ligado', 'Desligado') as ignicao,
      odometro
    FROM
      movimento_vl03
    WHERE serial = ${req.query.idV}
      AND dataGPS = '${req.query.dataGPS}'
  `)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_st390_detail', (req, res) => {
  pool.request().query(`
  SELECT TOP 1
    placa,
    dataGPS,
    localizacao,
    velocidade,
    IIF (ignicao = 1, 'Ligado', 'Desligado') as ignicao,
    tensao,
    odometro
  FROM movimento_suntech_st390
  WHERE idVeiculo = '${req.query.idV}'
  AND dataGPS = '${req.query.dataGps}'
  `)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_suntech410_detail', (req, res) => {
  pool.request().query(`
    SELECT TOP 1
      placa,
      dataGPS,
      localizacao,
      velocidade,
      IIF (online = 1, 'Ligado', 'Desligado') as ignicao,
      battery AS tensao,
      temperatura
    FROM movimento_suntech410
    WHERE idVeiculo = '${req.query.idV}'
    AND dataGPS = '${req.query.dataGps}'
  `)
  .then((result) => {
    res.send(result.recordset)
  })
});

app.get('/get_pos_hist_j16_detail', (req, res) => {
  pool.request().query(`
    SELECT TOP 1
      serial AS idVeiculo,
      id_cliente AS idCliente,
      placa,
      dataGPS,
      latitude, 
      longitude,
      endereco AS localizacao,
      velocidade,
      IIF ( ignicao = 1, 'Ligado', 'Desligado') AS ignicao,
      IIF ( ignicao = 1, '#00EB00', '#FF0322' ) AS statusVeic,
      tensao_bateria_interna AS tensao
    FROM movimento_j16
    WHERE serial = '${req.query.idV}'
    AND dataGPS = '${req.query.dataGps}'
  `)
  .then((result) => {
    res.send(result.recordset)
  })
});

// --------------------------------------------------------- COMMAND
app.post('/comando_suntech', async (req, res) => {
  var comando = req.query.comando;
  var dataGPS = dayjs().subtract(3,'hours').format("YYYY-MM-DD HH:mm:ss");
  let comandoSplited = comando.split(';')
  let serial = comandoSplited[1];
  let placa = await GetPlaca(serial); 
  let idClienteLog = parseInt(req.query.idClienteLog);
  let idUsuarioLog = 1;
  let comandoNome = await GetComandoNome(comandoSplited[3]);
  let numeroComum = gerarNumeroAleatorio()
  const axios = require('axios');
  
  let idCliente = await GetClienteSerial(serial);
  let phone = await GetNumeroChipSerial(serial)

  let idComando = await GetComando(comandoSplited[3])

  if (parseInt(idCliente) == 281) {
    if (idComando == 1){
  
      let api_key = 'MMAOYJX8JF38BPPDW9KD70O1LOJO6XB0U045SU8ESG1XQNUDFAGIO288ATD4H98EF7E3JYSPQLAXOJF0YZ6V0J85UHA93HS7OF1ALN25WRGCLO99TJ0HTGJ9PPDNFKXE';
  
      await axios.post(`https://api.smsdev.com.br/v1/send?key=${api_key}&type=9&number=${phone}&msg=${encodeURI(comando)}`)
        .then(function (response) {
        })
    } else {
      await pool.request().query(`insert into semaforo_suntech (data, comandoNome, placa, idClienteLog, idUsuarioLog, comando, serial, numeroComum) values ('${dataGPS}','${comandoNome}','${placa}', '${idClienteLog}', '${idUsuarioLog}', '${comando}', '${serial}', '${numeroComum}')`)
      .then( async result => {
        if (result.rowsAffected > 0) {

          await pool.request().query(`insert into bkp_semaforo_suntech (data, comandoNome, placa,idClienteLog,idUsuarioLog, comando ,serial , numeroComum) values ('${dataGPS}','${comandoNome}','${placa}', '${idClienteLog}', '${idUsuarioLog}', '${comando}', '${serial}', '${numeroComum}')`)
          .then (result => {
            if (result.rowsAffected > 0) {
              res.send('true');
            } else {
              res.send('false')
            }
          })
        } else {
          res.send('false')
        }
      })
    }
  } else {
    await pool.request().query(`insert into semaforo_suntech (data, comandoNome, placa,idClienteLog,idUsuarioLog, comando ,serial , numeroComum) values ('${dataGPS}','${comandoNome}','${placa}', ${idClienteLog}, ${idUsuarioLog}, '${comando}', ${serial}, '${numeroComum}')`)
    .then(async result => {
      if (result.rowsAffected > 0) {

        await pool.request().query(`insert into bkp_semaforo_suntech (data, comandoNome, placa,idClienteLog,idUsuarioLog, comando ,serial , numeroComum) values ('${dataGPS}','${comandoNome}','${placa}', ${idClienteLog}, ${idUsuarioLog}, '${comando}', ${serial}, '${numeroComum}')`)
        .then(result => {
          if (result.rowsAffected > 0) {
            res.send('true')
          } else {
            res.send('false')
          }
        })
      } else {
        res.send('false')
      }
    });
  }
});

app.post('/comando_gv75', async (req, res) => {
  var comando = req.query.comando;
  var data = dayjs().subtract(3,'hours').format("YYYY-MM-DD HH:mm:ss");
  let comandoNome = '';
  var placa = req.query.placa;
  let idRastreador = await GetSerial(placa)
  let idUsuarioLog = 1
  let idCliente = await GetClienteSerial(idRastreador);
  let numeroComum = gerarNumeroAleatorio();

  if(comando.includes('250')){
    comandoNome = 'Desativar imobilizador';
  }else if(comando.includes('63')){
    comandoNome = 'Ativar imobilizador';
  }
  
  await pool.request().query(`insert into semaforo_gv75 (data, placa,idRastreador, comando, idUsuarioLog, idClienteLog, comando_nome, numeroComum) values ('${data}', '${placa}','${idRastreador}', '${comando}',${idUsuarioLog},${idCliente},'${comandoNome}', ${numeroComum})`)
    .then(async result => {
      if (result.rowsAffected > 0) {

        await pool.request().query(`insert into bkp_semaforo_gv75 (data, placa,idRastreador, comando, idUsuarioLog, idClienteLog, comando_nome, numeroComum) values ('${data}', '${placa}','${idRastreador}', '${comando}',${idUsuarioLog},${idCliente},'${comandoNome}', ${numeroComum})`)
        .then(result => {
          if (result.rowsAffected > 0) {
            res.send('true')
          } else {
            res.send('false')
          }
        })
      } else {
        res.send('false')
      }
    });
});

app.post('/comando_mt2000', async (req, res) => {
  var comando = req.query.comando;
  var placa = req.query.placa;
  var data = req.query.data;
  var comandoNome = req.query.comandoNome;
  var idClienteLog = req.query.idClienteLog;
  var idUsuarioLog = req.query.idUsuarioLog;
  var serial = req.query.serial;

  await pool.request().query(`insert into semaforo_mt2000 (data, placa, comando, comandoNome, idClienteLog, idUsuarioLog, serial) values ('${data}', '${placa}', '${comando}', '${comandoNome}', ${idClienteLog}, ${idUsuarioLog}, '${serial}')`)
  .then( async (result) => {
    if (result.rowsAffected > 0) {

      await pool.request().query(`insert into bkp_semaforo_mt2000 (data, placa, comando, comandoNome, idClienteLog, idUsuarioLog, serial) values ('${data}', '${placa}', '${comando}', '${comandoNome}', ${idClienteLog}, ${idUsuarioLog}, '${serial}')`)
      .then(result => {
        if (result.rowsAffected > 0) {
          res.send('true');
        } else {
          res.send('false');
        }
      })
    } else {
      res.send('false');
    }
  });
});

app.post('/comando_teltonika', async (req, res) => {
  var dataGPS = req.query.dataGPS;
  var comando = req.query.comando;
  var comandoNome = req.query.comandoNome;
  var placa = req.query.placa;
  var idClienteLog = req.query.idClienteLog;
  var idUsuarioLog = req.query.idUsuarioLog;
  var idVeiculo = req.query.idVeiculo;

  await pool.request().query(`insert into semaforo_teltonika (data, comando, comandoNome, placa, idClienteLog, idusuarioLog, serial) values ('${dataGPS}', '${comando}', '${comandoNome}', '${placa}', ${idClienteLog}, ${idUsuarioLog}, ${idVeiculo})`)
  .then(async result => {
    if (result.rowsAffected > 0) {
      res.send('true');
    } else {
      res.send('false');
    }
  });
});

app.post('/comando_vl03', async (req, res) => {
  var dataGPS = req.query.dataGPS;
  var comando = req.query.comando;
  var comandoNome = req.query.comandoNome;
  var retorno = req.query.retorno;
  var placa = req.query.placa;
  var idClienteLog = req.query.idClienteLog;
  var idUsuarioLog = req.query.idUsuarioLog;
  var idVeiculo = req.query.idVeiculo;

  await pool.request().query(`insert into semaforo_vl03 (data, comando, comandoNome, retorno, placa, idClienteLog, idusuarioLog, serial) values ('${dataGPS}', '${comando}', '${comandoNome}', '${retorno}','${placa}', ${idClienteLog}, ${idUsuarioLog}, ${idVeiculo})`)
  .then(async result => {
    if (result.rowsAffected > 0) {
      await pool.request().query(`insert into bkp_semaforo_vl03 (data, comando, comandoNome, retorno, placa, idClienteLog, idusuarioLog, serial) values ('${dataGPS}', '${comando}', '${comandoNome}', '${retorno}','${placa}', ${idClienteLog}, ${idUsuarioLog}, ${idVeiculo})`)
      .then(result => {
        if (result.rowsAffected > 0) {
          res.send('true');
        } else {
          res.send('false');
        }
      })
    } else {
      res.send('false');
    }
  });
});

app.post('/comando_j16', async (req, res) => {
  var dataGPS = req.query.dataGPS;
  var comando = req.query.comando;
  var comandoNome = req.query.comandoNome;
  var retorno = req.query.retorno;
  var placa = req.query.placa;
  var idClienteLog = req.query.idClienteLog;
  var idUsuarioLog = req.query.idUsuarioLog;
  var idVeiculo = req.query.idVeiculo;

  await pool.request().query(`insert into semaforo_j16 (data, comando, comandoNome, retorno, placa, idClienteLog, idusuarioLog, serial) values ('${dataGPS}', '${comando}', '${retorno}', '${comandoNome}','${placa}', ${idClienteLog}, ${idUsuarioLog}, ${idVeiculo})`)
  .then(async result => {
    
    if (result.rowsAffected > 0) {
      await pool.request().query(`insert into bkp_semaforo_j16 (data, comando, comandoNome, retorno, placa, idClienteLog, idusuarioLog, serial) values ('${dataGPS}', '${comando}', '${retorno}', '${comandoNome}','${placa}', ${idClienteLog}, ${idUsuarioLog}, ${idVeiculo})`)
      .then(result => {
        if (result.rowsAffected > 0) {
          res.send('true');
        } else {
          res.send('false');
        }
      })
    } else {
      res.send('false');
    }
  });
});

// --------------------------------------------------------- ANCHOR
app.post('/post_anchor', async (req, res) => {
  let idVeiculo = req.query.idVeiculo;
  let dataGPS = req.query.dataGPS;
  let idCliente = req.query.idCliente;
  let localizacao = req.query.localizacao;
  let tipoRaio = req.query.tipoRaio;
  let raio = req.query.raio;
  let longitude = req.query.longitude;
  let latitude = req.query.latitude;
  let ancorado = req.query.ancorado;
  let placa = req.query.placa;

  await pool.request()
    .input('dataGPS', sql.DateTime, `${dataGPS}`)
    .input('idVeiculo', sql.VarChar, `${idVeiculo}`)
    .input('idCliente', sql.VarChar, `${idCliente}`)
    .input('localizacao', sql.VarChar, `${localizacao}`)
    .input('tipoRaio', sql.VarChar, `${tipoRaio}`)
    .input('raio', sql.VarChar, `${raio}`)
    .input('longitude', sql.VarChar, `${longitude}`)
    .input('latitude', sql.VarChar, `${latitude}`)
    .input('ancorado', sql.VarChar, `${ancorado}`)
    .input('placa', sql.VarChar, `${placa}`)
    .query(
      `insert into ancora (idVeiculo, dataGPS, idCliente, localizacao, tipoRaio, raio, longitude, latidute, ancorado, placa) 
        values (@idVeiculo, @dataGPS, @idCliente, @localizacao, @tipoRaio, @raio, @longitude, @latitude, @ancorado, @placa)`
    )
    .then(result => {
      if (result.rowsAffected > 0) {
        res.send('true');
      } else {
        res.send('false');
      }
    });
});

app.get('/get_anchor', (req, res) => {
  execSQLQuery(`
    SELECT * FROM ancora
    WHERE placa = '${req.query.placa}'
    AND ancorado = 1`,
    res
  );
});

app.get('/get_anchors', (req, res) => {
  if (req.query.placa) {
    let places = req.query.placa.indexOf(',') > 0 ? req.query.placa.replace(/,/g,"','") : req.query.placa
    pool.request().query(`
      SELECT * FROM 
        ( SELECT DISTINCT
            *,
            ROW_NUMBER() OVER (PARTITION BY placa ORDER BY dataGPS DESC ) AS RowNumber
          FROM ancora
          WHERE placa in ('${places}')
            AND ancorado=1
        ) AS a
      WHERE a.RowNumber = 1
    `)
    .then((result) => {
      res.send(result.recordset)
    })
  }
});

app.put('/put_anchor', async (req, res) => {
  let idAncora = req.query.idAncora;

  await poolConnect;
  await pool.request()
    .input('idAncora', sql.Int, `${idAncora}`)
    .query(
      `UPDATE ancora
      SET ancorado = 0
      WHERE id = ${idAncora};`
    )
    .then(result => {
      if (result.rowsAffected > 0) {
        res.send('true');
      } else {
        res.send('false');
      }
    });
});

const brokenAnchor = async () => {  
  await pool.request().query(`
    SELECT
      a.placa,
      a.serial,
      b.idmodelo,
      c.descModelo
    FROM
      ancora x
      INNER JOIN cadastro_veiculo a ON a.placa=x.placa
      INNER JOIN cadastro_rastreador b ON b.serial=a.serial
      INNER JOIN modelo c ON c.id = b.idmodelo
    WHERE
      x.ancorado=1
  `)
  .then( async (result) => {
    for (let i = 0; i < result.recordset.length; i++) {
      let tech = result.recordset[i].descModelo
      let tech2 = (result.recordset[i].descModelo).substring(0,2)
      let dataGPS = '', idCliente

      await pool.request().query(`
        SELECT TOP 1
          latitude,
          longitude,
          ${tech2 == 'GV' ? 'sendtime as dataGPS' : 'dataGPS'},
          ${tech2 == 'VL' ? 'id_cliente as idCliente' : 'idCliente'}
        FROM 
          ${
            tech2 == 'ST'
              ? tech == 'ST390'
                ? 'movimento_suntech_st390_frontend '
                : 'movimento_suntech_frontend'
              : tech2 == 'MT'
                ? 'movimento_mt2000_frontend'
                : tech2 == 'GR'
                  ? 'movimento_grcad_frontend'
                  : tech2 == 'Te'
                    ? 'movimento_teltonika_frontend'
                    : tech2 == 'VL'
                      ? 'movimento_vl03_frontend'
                      : 'movimento_gv75_frontend'
          }
        WHERE
          placa='${result.recordset[i].placa}'
          AND latitude <> ''
          AND longitude <> ''
        ORDER BY ${tech2 == 'GV' ? 'sendtime' : 'dataGPS'} DESC
      `)
      .then( async (result2) => {
        if (result2.recordset.length > 0) {
          idCliente = result2.recordset[0].idCliente || 0

          if (result2.recordset[0].dataGPS.length == undefined) {
            let temp
            temp = (result2.recordset[0].dataGPS).toISOString()
            dataGPS = await String(temp.substring(0,10) + ' ' + temp.substring(11,19))
          } else {
            dataGPS = await result2.recordset[0].dataGPS
          }

          await pool.request().query(`
            SELECT
              a.placa,
              a.serial,
              x.latidute,
              x.longitude,
              IIF (  x.tipoRaio = 'km', x.raio,
                IIF ( LEN(x.raio) = 1, '0.00' + x.raio,
                  IIF ( LEN(x.raio) = 2, '0.0' + x.raio,
                    IIF ( LEN(x.raio) = 3, '0.' + x.raio,
                      IIF ( LEN(x.raio) > 3, x.raio, '0')
                    )
                  )
                )
              ) AS radius,
              (6371 * acos (
                cos(radians(x.latidute)) * 
                cos(radians(${tech2 == 'VL' ? result2.recordset[0].latitude * -1 : result2.recordset[0].latitude})) * 
                cos(radians(x.longitude) - radians(${tech2 == 'VL' ? result2.recordset[0].longitude * -1 : result2.recordset[0].longitude})) + 
                sin(radians(x.latidute)) * 
                sin(radians(${tech2 == 'VL' ? result2.recordset[0].latitude * -1 : result2.recordset[0].latitude}))
              )) AS distancia,
              b.idmodelo,
              c.descModelo
            FROM
              ancora x
              INNER JOIN cadastro_veiculo a ON a.placa=x.placa
              INNER JOIN cadastro_rastreador b ON b.serial=a.serial
              INNER JOIN modelo c ON c.id = b.idmodelo
            WHERE
              x.ancorado=1
              AND a.placa = '${result.recordset[i].placa}'
            ORDER BY
              c.descModelo
            ASC
          `)
          .then( async (result3) => {
                
            await pool.request().query(`SELECT a.id_usuario as idUsuario FROM assoc_veiculos_usuarios a INNER JOIN cadastro_veiculo b ON b.id = a.id_veiculo WHERE b.placa = '${result3.recordset[0].placa}' `)
            .then( async (result) => {
              for (let x = 0; x < result.recordset.length; x++) {

                if(result3.recordset[0].distancia > result3.recordset[0].radius) {
                  let title = 'Âncora violada'
                  let body = `O veículo ${result3.recordset[0].placa} violou a âncora estabelecida.`

                  await pool.request().query(`
                    SELECT * FROM notificacoes WHERE idUsuario=${result.recordset[x].idUsuario} AND idEvento = 'Âncora violada' AND placa = '${result3.recordset[0].placa}'
                  `)
                  .then( async (result4) => {
                    if (result4.recordset.length == 0) {
                      
                      await pool.request().query(`
                        INSERT INTO notificacoes (dataGPS, idCliente, idEvento, idUsuario, placa) VALUES ('${String(dataGPS)}', '${idCliente}', '${title}', '${result.recordset[x].idUsuario}', '${result3.recordset[0].placa}')
                      `)
                      .then( async (results) => {
  
                        await pool.request().query(`
                          SELECT fcm FROM login_app where idUsuario='${result.recordset[x].idUsuario}'
                        `)
                        .then(async (result) => {
                          for (let y = 0; y < result.recordset.length; y++) {
                            await sendNotification(result.recordset[y].fcm, title, body)                                      
                          }
                        })

                      })

                    }
                  })
                } else {
                  let title = 'Âncora reestabelecida'
                  let body = `O veículo ${result3.recordset[0].placa} voltou à âncora estabelecida.`

                  await pool.request().query(`
                    SELECT * FROM notificacoes WHERE idUsuario=${result.recordset[x].idUsuario} AND idEvento = 'Âncora violada' AND placa = '${result3.recordset[0].placa}'
                  `)
                  .then( async (result5) => {
                    if (result5.recordset.length > 0) {

                      await pool.request().query(`
                        SELECT * FROM notificacoes WHERE idUsuario=${result.recordset[x].idUsuario} AND idEvento = 'Âncora reestabelecida' AND placa = '${result3.recordset[0].placa}'
                      `)
                      .then( async (result4) => {
                        if (result4.recordset.length == 0) {
                          
                          await pool.request().query(`
                            INSERT INTO notificacoes (dataGPS, idCliente, idEvento, idUsuario, placa) VALUES ('${dataGPS}', ${idCliente}, '${title}', '${result.recordset[x].idUsuario}', '${result3.recordset[0].placa}')
                          `)
                          .then( async (results) => {
      
                            await pool.request().query(`
                              SELECT fcm FROM login_app where idUsuario='${result.recordset[x].idUsuario}'
                            `)
                            .then( async (result) => {

                              for (let y = 0; y < result.recordset.length; y++) {            
                                await sendNotification(result.recordset[y].fcm, title, body)
                              }

                            })
                          })

                        }
                      })

                    }
                  })

                }
                
              }
            })

          })

        }
      })
    }

  })
} 

// --------------------------------------------------------- VEÃƒÂCULO

app.get('/truck_detail', (req, res) => {
  execSQLQuery(`
    SELECT
      id,
      placa,
      marca,
      Modelo,
      cor,
      ano,
      idCliente,
      socket,
      serial,
      etiqueta,
      idCategoria,
      idUsuario,
      pinTrac,
      tipoCombustivel,
      valorCombustivel,
      kmLitroCombustivel
    FROM
      cadastro_veiculo
    WHERE
      placa = '${req.query.plate}'
  `, res)
})

app.put('/truck_detail', async (req, res) => {
  await poolConnect;
  await pool.request()
  .query(
    `UPDATE cadastro_veiculo
    SET tipoCombustivel = '${req.query.type}',
      valorCombustivel = '${req.query.value}',
      kmLitroCombustivel = '${req.query.kmLiter}'
    WHERE placa = '${req.query.plate}';`
  )
  .then(result => {
    if (result.rowsAffected > 0) {
      res.send('true');
    } else {
      res.send('false');
    }
  });
});

// --------------------------------------------------------- REFERENCE
app.get('/get_classification', (req, res) => {
  execSQLQuery(`
    SELECT
      id,
      nome
    FROM
      classificacao
    WHERE
      idCliente in (${req.query.id})
    ORDER BY
      nome
  `, res
  );
});

app.post('/post_reference', async (req, res) => {
  await pool.request().query(`
    INSERT INTO ponto_referencia (cliente, nomeReferencia, latitude, longitude, classificacao, localizacao, idCliente, raio)
    VALUES ('${req.query.customer}', '${req.query.nameReference}', ${req.query.latitude}, ${req.query.longitude}, '${req.query.classification}', '${req.query.localization}', ${req.query.idCustomer}, ${req.query.radius})
  `)
  .then(result => {
    if (result.rowsAffected) {
      res.send('true');
    } else {
      res.send('false');
    }
  })
  .catch(err => {
    res.send(false)
  })
});

app.put('/put_reference', async (req, res) => {
  await pool.request().query(`
    UPDATE ponto_referencia
    SET nomeReferencia='${req.query.name}', latitude=${req.query.latitude}, longitude=${req.query.longitude}, localizacao='${req.query.localization}', raio=${req.query.radius}
    WHERE id=${req.query.id}
  `)
  .then(result => {
    if (result.rowsAffected > 0) {
      res.send(true);
    } else {
      res.send(false);
    }
  })
  .catch(err => {
    res.send(false)
  })
});

app.delete('/delete_reference', async (req, res) => {
  await pool.request().query(`
    DELETE FROM ponto_referencia WHERE id=${req.query.id}
  `)
  .then(result => {
    if (result.rowsAffected > 0) {
      res.send(true);
    } else {
      res.send(false);
    }
  })
  .catch(err => res.send(false))
});

app.get('/get_reference', (req, res) => {
  execSQLQuery(`
    SELECT
      id,
      cliente,
      nomeReferencia,
      latitude,
      longitude,
      classificacao,
      localizacao,
      raio
    FROM
      ponto_referencia
    WHERE
      idCliente in (${req.query.id})
    ORDER BY
      id DESC
  `, res);
});

app.get('/get_references', (req, res) => {
  execSQLQuery(`
    SELECT
      id,
      cliente,
      nomeReferencia,
      latitude,
      longitude,
      classificacao,
      localizacao,
      raio
    FROM
      ponto_referencia
    WHERE
      idCliente in (${req.query.id})
      AND raio IS NOT NULL
      AND raio NOT LIKE '%k%'
      AND raio NOT LIKE '%m%'
      AND raio NOT LIKE '%K%'
      AND raio NOT LIKE '%M%'
    ORDER BY
      id DESC
  `, res);
});

//-------------------------------------------------NOTIFICATION
app.post('/post_notification_adm', async (req, res) => {
  const msg = req.body

  pool.request().query(`
    SELECT idUsuario FROM new_usuario WHERE idUsuario <> 1 ORDER BY idUsuario ASC
  `)
  .then((result) => {
    
    for (let i = 0; i < result.recordset.length;i++) {
      pool.request().query(`
        INSERT INTO notificacoes (dataGPS, CustomTitle, CustomBody, idUsuario) VALUES ('${msg.dataGPS}', '${msg.title}', '${msg.body}', '${result.recordset[i].idUsuario}')
      `)
      .then((results) => {
        
        pool.request().query(`
          SELECT fcm FROM login_app where idUsuario='${result.recordset[i].idUsuario}'
        `)
        .then( async result => {
          for (let i = 0; i < result.recordset.length; i++) {
            await sendNotification(result.recordset[i].fcm, msg.title, msg.body)
          }
        })
        .catch(err => console.log('ERRO ENVIO NOTFICACAO ADM', err))

      })
      .catch(err => console.log('ERRO REGISTRO NOTFICACAO ADM', err))
    }
  })

  res.send(true)
})

app.get('/get_notifications', async (req, res) => {
  let notifications = []
  await pool.request()
  .query(`SELECT * FROM notificacoes WHERE idUsuario = '${req.query.user}' ORDER BY dataGPS DESC`)
  .then((result) => {
    if (result.recordset.length > 0) {
      for (let y = 0; y < result.recordset.length; y++) {
        notifications.push(result.recordset[y])
      }
    }
  })
  
  res.send(notifications)
})

app.get('/get_notification_place', async (req, res) => {
  await pool.request()
  .query(`SELECT * FROM notificacoes WHERE placa='${req.query.place}' AND idUsuario = '${req.query.user}' ORDER BY dataGPS DESC`)
  .then((result) => {
    res.send(result.recordset)
  })
})

app.get('/delete_notifications', async (req, res) => {
  await pool.request()
  .query(`DELETE FROM notificacoes WHERE id=${req.query.id}`)
  .then((result) => {
    res.send(true)
  })
  .catch((err) => {
    res.send(false)
  })
})

app.get('/set_login_app', async (req, res) => {
  var fcm = req.query.fcm;
  var idUser = req.query.idUser;
  var data = req.query.data;

  await pool.request()
  .query(`SELECT * FROM login_app WHERE fcm='${req.query.fcm}'`)
  .then( async (result) => {
    if (result.recordset.length > 0) {
      await pool.request()
      .query(`UPDATE login_app set idUsuario='${req.query.idUser}', data='${req.query.data}' WHERE fcm='${req.query.fcm}'`)
      .then( result => {
        res.send('true')
      })
    } else {
      await pool.request()
      .input('fcm', sql.VarChar, `${fcm}`)
      .input('idUser', sql.Int, `${idUser}`)
      .input('date', sql.VarChar, `${data}`)
      .query(`insert into login_app (fcm, idUsuario,data) values (@fcm, @idUser, @date)`)
      .then(result => {
        if (result.rowsAffected > 0) {
          res.send('true');
        } else {
          res.send('false');
        }
      });
    }
  })
})

app.get('/delete_login_app', async (req, res) => {
  await pool.request()
  .query(`DELETE FROM login_app WHERE fcm='${req.query.fcm}'`)
  .then( async (result) => {
    res.send('true')
  })
})

app.get('/version_app', async (req, res) => {
  if (req.query.platform == 'android') {
    await pool.request().query(`SELECT versao FROM app_version`)
    .then( result => {
      res.send(result.recordset[0].versao > req.query.version)
    })
  } else {
    await pool.request().query(`SELECT versaoIOS FROM app_version`)
    .then( result => {
      res.send(result.recordset[0].versao > req.query.version)
    })
  }
})

//COMMANDS REPORT
app.get('/report_commands_suntech', async (req, res) => {
  let places = req.query.place.replace(/,/g,`','`)

  await pool.request().query(`
    SELECT
      a.data,
      a.comandoNome,
      a.placa,
      a.id,
      b.nome
    FROM
      bkp_semaforo_suntech a
    INNER JOIN
      new_usuario b ON b.idUsuario = a.idUsuarioLog
    WHERE
      a.placa in ('${places}')
    AND
      a.data BETWEEN '${req.query.dtI}' AND '${req.query.dtF}'
    ORDER BY data DESC
  `)
  .then( result => {
    res.send(result.recordset)
  })
})

app.get('/report_commands_suntech_st390', async (req, res) => {
  let places = req.query.place.replace(/,/g,`','`)

  await pool.request().query(`
    SELECT
      a.data,
      a.comandoNome,
      a.placa,
      a.id,
      b.nome
    FROM
      bkp_semaforo_suntech_st390 a
    INNER JOIN
      new_usuario b ON b.idUsuario = a.idUsuarioLog
    WHERE
      a.placa in ('${places}')
    AND
      a.data BETWEEN '${req.query.dtI}' AND '${req.query.dtF}'
    ORDER BY data DESC
  `)
  .then( result => {
    res.send(result.recordset)
  })
})

app.get('/report_commands_suntech_380', async (req, res) => {
  let places = req.query.place.replace(/,/g,`','`)

  await pool.request().query(`
    SELECT
      a.data,
      a.comandoNome,
      a.placa,
      a.id,
      b.nome
    FROM
      bkp_semaforo_suntech380 a
    INNER JOIN
      new_usuario b ON b.idUsuario = a.idUsuarioLog
    WHERE
      a.placa in ('${places}')
    AND
      a.data BETWEEN '${req.query.dtI}' AND '${req.query.dtF}'
    ORDER BY data DESC
  `)
  .then( result => {
    res.send(result.recordset)
  })
})

app.get('/report_commands_suntech_410', async (req, res) => {
  let places = req.query.place.replace(/,/g,`','`)

  await pool.request().query(`
    SELECT
      a.data,
      a.comandoNome,
      a.placa,
      a.id,
      b.nome
    FROM
      bkp_semaforo_suntech410 a
    INNER JOIN
      new_usuario b ON b.idUsuario = a.idUsuarioLog
    WHERE
      a.placa in ('${places}')
    AND
      a.data BETWEEN '${req.query.dtI}' AND '${req.query.dtF}'
    ORDER BY data DESC
  `)
  .then( result => {
    res.send(result.recordset)
  })
})

app.get('/report_commands_gv75', async (req, res) => {
  let places = req.query.place.replace(/,/g,`','`)

  await pool.request().query(`
    SELECT
      a.data,
      a.comando_nome AS comandoNome,
      a.placa,
      a.id,
      b.nome
    FROM
      bkp_semaforo_gv75 a
    INNER JOIN
      new_usuario b ON b.idUsuario = a.idUsuarioLog
    WHERE
      a.placa in ('${places}')
    AND
      a.data BETWEEN '${req.query.dtI}' AND '${req.query.dtF}'
    ORDER BY data DESC
  `)
  .then( result => {
    res.send(result.recordset)
  })
})

app.get('/report_commands_teltonika', async (req, res) => {
  let places = req.query.place.replace(/,/g,`','`)

  await pool.request().query(`
    SELECT
      a.data,
      a.comandoNome,
      a.placa,
      a.id,
      b.nome
    FROM
      bkp_semaforo_teltonika a
    INNER JOIN
      new_usuario b ON b.idUsuario = a.idUsuarioLog
    WHERE
      a.placa in ('${places}')
    AND
      a.data BETWEEN '${req.query.dtI}' AND '${req.query.dtF}'
    ORDER BY data DESC
  `)
  .then( result => {
    res.send(result.recordset)
  })
})

app.get('/report_commands_mt2000', async (req, res) => {
  let places = req.query.place.replace(/,/g,`','`)

  await pool.request().query(`
    SELECT
      a.data,
      a.comandoNome,
      a.placa,
      a.id,
      b.nome
    FROM
      bkp_semaforo_mt2000 a
    INNER JOIN
      new_usuario b ON b.idUsuario = a.idUsuarioLog
    WHERE
      a.placa in ('${places}')
    AND
      a.data BETWEEN '${req.query.dtI}' AND '${req.query.dtF}'
    ORDER BY data DESC
  `)
  .then( result => {
    res.send(result.recordset)
  })
})

app.get('/report_commands_vl01', async (req, res) => {
  let places = req.query.place.replace(/,/g,`','`)

  await pool.request().query(`
    SELECT
      a.data,
      a.comandoNome,
      a.placa,
      a.id,
      b.nome
    FROM
      bkp_semaforo_vl01 a
    INNER JOIN
      new_usuario b ON b.idUsuario = a.idUsuarioLog
    WHERE
      a.placa in ('${places}')
    AND
      a.data BETWEEN '${req.query.dtI}' AND '${req.query.dtF}'
    ORDER BY data DESC
  `)
  .then( result => {
    res.send(result.recordset)
  })
})

app.get('/report_commands_vl02', async (req, res) => {
  let places = req.query.place.replace(/,/g,`','`)

  await pool.request().query(`
    SELECT
      a.data,
      a.comandoNome,
      a.placa,
      a.id,
      b.nome
    FROM
      bkp_semaforo_vl02 a
    INNER JOIN
      new_usuario b ON b.idUsuario = a.idUsuarioLog
    WHERE
      a.placa in ('${places}')
    AND
      a.data BETWEEN '${req.query.dtI}' AND '${req.query.dtF}'
    ORDER BY data DESC
  `)
  .then( result => {
    res.send(result.recordset)
  })
})

app.get('/report_commands_vl03', async (req, res) => {
  let places = req.query.place.replace(/,/g,`','`)

  await pool.request().query(`
    SELECT
      a.data,
      a.comandoNome,
      a.placa,
      a.id,
      b.nome
    FROM
      bkp_semaforo_vl03 a
    INNER JOIN
      new_usuario b ON b.idUsuario = a.idUsuarioLog
    WHERE
      a.placa in ('${places}')
    AND
      a.data BETWEEN '${req.query.dtI}' AND '${req.query.dtF}'
    ORDER BY data DESC
  `)
  .then( result => {
    res.send(result.recordset)
  })
})

// const httpServer = http.createServer(app);
//  httpServer.listen(3000, () => {
//   console.log("HTTP Server running on port 16003");
// });

httpsServer.listen(88, () => {
  // console.log('HTTPS Server running on port 88');
  console.log("||| SERVIDOR CONECTADO |||")
});

const intervalNotification = setInterval(() => {
  processNotification()
  brokenAnchor()
}, 30 * 1000)
return () => clearInterval(intervalNotification)
console.log('')