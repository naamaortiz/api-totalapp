const firebase = require("firebase/app")
const database = require("firebase/database")

firebase.initializeApp({
    apiKey: "AIzaSyAItRpNlrJ1HlpImQ8hoFoQe1zsHvXw1CE",
    authDomain: "total-trac.firebaseapp.com",
    databaseURL: "https://total-trac-default-rtdb.firebaseio.com",
    projectId: "total-trac",
    storageBucket: "total-trac.appspot.com",
    messagingSenderId: "732437184139",
    appId: "1:732437184139:web:de8cf98538832bdf695e8e",
    measurementId: "G-HCH6ER65NL"
});